  Formation standard ParaView+ : 3 matinées (un peu plus d'une dizaine)
+ Formation pratique experte : 1 matinée (quelques personnes).

Fournir exécutables téléchargeable Windows et Linux, jeux de données VTK.

Les stagiaires auront l'après-midi en solitaire pour manipuler abondamment.

Utilisation des bases du Poste 1.1 ?

Points d'intérêt à ne pas rater :
- le reader Hercule retourne un objet VTK :
  - un multi-block data set pour décrire différentes représentations (pas systématique aujourd'hui)
  - un multi-block data set pour décrire un matériau / partie
  - un multi-block data piece pour décrire chaque sous-domaine de calcul
  - le data set (ou data object dans le cas de l'AMR) avec
    - champ de valeurs Field
    - champ de valeurs Cell
    - champ de valeurs Point

- affichage d'un champ de valeurs sur les faces des cellules et en même temps une coloration des arêtes (couleur fixée ou couleur donnée par un autre champ de valeurs)

- affichage d'un champ de valeurs sur les faces des cellules et "features edges" (silhouette) avec une coloration (couleur fixée ou par un autre champ de valeurs)

- colorisé par un champ de valeurs Fields (vtkMaterial) surface, arête ou "features edges"

- produire un champ de vecteurs à partir d'une grandeur vectorielle

- sélection (par id, spatiale), déselection

- à partir d'une lecture, coloriser un matériau suivant une grandeur et un autre matériau suivant une autre grandeur

- annotation (dév. fev21)

- aborder la notion de script batch
