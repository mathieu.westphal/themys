# Prise en compte du boutton Abort (P4)
L'idée est qu'un filtre *check* à intervalle régulier si le bouton **abort** a été appuyé, activé.

Reste au filtre de faire ce *check* à un moment de l'exécution du filtre pour lequel il peut sauter les actions restantes à réaliser afin de sortir au plus vite de l'exécution de ce dernier.

Pour un filtre reader, cela revient à ne pas lire toutes les données.
Pour un filtre de traitement, cela revient à ne pas réaliser tous les traitements.

Une fois le rendu réalisé, le bouton **abort** est automatiquement désactivé.

Hors ParaView, le "check" retourne toujours faux.
