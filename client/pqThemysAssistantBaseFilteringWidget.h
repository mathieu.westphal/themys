/**
 * File defining all custom widgets used in the Assistant dialog of Themys.
 */

#ifndef pqThemysAssistantWidgets_h
#define pqThemysAssistantWidgets_h

#include <QWidget>

#include "pqThemysAssistantBaseManager.h"

class QAbstractItemModel;
class QComboBox;
class pqThemysAssistantBaseModel;
class pqThemysAssistantStorageModel;
// ============================================================================
class pqThemysAssistantBaseFilteringWidget final : public QWidget
{
  typedef QWidget Superclass;
  Q_OBJECT
public:
  pqThemysAssistantBaseFilteringWidget(QWidget* p = nullptr);
  ~pqThemysAssistantBaseFilteringWidget() override;
  void Initialize(pqThemysAssistantBaseManager* manager,
                  pqThemysAssistantStorageModel* storageModel,
                  pqThemysAssistantBaseModel* baseModel);

Q_SIGNALS:
  /**
   * Emitted whenever the user has requested to find some bases, either by
   * filtering or by recently used. See onSearchFilteredBases() and
   * onSearchRecentBases().
   */
  void preFilterBases();
  void showFilteredBases(QVector<assistant::Base> bases);
  void showRecentBases(QVector<assistant::Base> bases);

public Q_SLOTS:
  void onStorageSelected(int index);
  void onUserSelected(int index);
  void onUserEditTextChanged();
  void onSearchFilteredBases();
  void onSearchRecentBases();

private:
  void loadPreviousSession();

  struct pqInternals;
  QScopedPointer<pqInternals> Internals;
};

#endif // pqThemysAssistantWidgets_h
