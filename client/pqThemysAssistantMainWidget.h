#ifndef pqThemysAssistantMainWidget_h
#define pqThemysAssistantMainWidget_h

#include <QScopedPointer>
#include <QWidget>

/**
 * A widget containing all the assistant widget and logic
 */
class pqServer;
class pqThemysAssistantMainWidget : public QWidget
{
  typedef QWidget Superclass;
  Q_OBJECT
public:
  pqThemysAssistantMainWidget(pqServer* server, QWidget* parent);
  ~pqThemysAssistantMainWidget() override;

  /**
   * Manual status of the assistant
   */
  void setToggleAssistantStatus(bool toggle);

Q_SIGNALS:
  void toggleAssistantClicked(bool toggle);
  void baseAccepted(const QString& base);
  void cancelClicked();

protected:
  // Called when user accept a base
  bool acceptBases();

private:
  pqThemysAssistantMainWidget(const pqThemysAssistantMainWidget&);
  pqThemysAssistantMainWidget& operator=(const pqThemysAssistantMainWidget&);

  struct pqInternals;
  QScopedPointer<pqInternals> Internals;

private Q_SLOTS:

  // Called when the user right-clicks in the file qtreeview
  void onContextMenuRequested(const QPoint& pos);

  // Called when the user changes the file selection.
  void fileSelectionChanged();

  //@{
  /**
   * Slots for handling study creation, deletion and selection.
   */
  void onCreateStudy() const;
  void onRemoveSelectedStudy() const;
  void onActivateStudy(const QModelIndex& idx) const;
  void addSelectedBaseToStudy(const QModelIndex& study) const;
  void removeSelectedBaseFromActiveStudy() const;
  //@}
};

#endif // !pqThemysAssistantMainWidget_h
