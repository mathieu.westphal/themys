#include "pqThemysAssistantReaction.h"

#include <vtksys/SystemTools.hxx>

#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqObjectBuilder.h"
#include "pqPipelineSource.h"
#include "pqServer.h"
#include "pqThemysAssistantFileDialog.h"
#include "vtkSMCoreUtilities.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMProxy.h"
#include "vtkSMProxyManager.h"
#include "vtkSMReaderFactory.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMSourceProxy.h"

//-----------------------------------------------------------------------------
pqThemysAssistantReaction::pqThemysAssistantReaction(QAction* parentObject)
    : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
QList<pqPipelineSource*> pqThemysAssistantReaction::loadData()
{
  pqServer* server = pqActiveObjects::instance().activeServer();
  vtkSMReaderFactory* readerFactory =
      vtkSMProxyManager::GetProxyManager()->GetReaderFactory();
  std::vector<FileTypeDetailed> filtersDetailed =
      readerFactory->GetSupportedFileTypesDetailed(server->session());

  QString filtersString;
  bool first = true;
  // Generates the filter string used by the fileDialog
  // For example, this could be "Supported Files (*.jpg *.jpeg *.png);;All Files
  // (*);;JPEG Image Files(*.jpg *.jpeg);;PNG Image Files (*.png)"
  for (auto const& filterDetailed : filtersDetailed)
  {
    if (!first)
    {
      filtersString += ";;";
    }

    filtersString += QString::fromStdString(filterDetailed.Description) + " (" +
                     QString::fromStdString(vtksys::SystemTools::Join(
                         filterDetailed.FilenamePatterns, " ")) +
                     ")";

    first = false;
  }

  int constexpr SupportedFilesFilterIndex = 0;
  int constexpr AllFilesFilterIndex = 1;

  pqThemysAssistantFileDialog fileDialog(server, pqCoreUtilities::mainWidget(),
                                         tr("Open File:"), QString(),
                                         filtersString);
  QList<pqPipelineSource*> sources;
  fileDialog.setObjectName("FileOpenDialog");
  fileDialog.setFileMode(
      pqThemysAssistantFileDialog::ExistingFilesAndDirectories);
  if (fileDialog.exec() == QDialog::Accepted)
  {
    QList<QStringList> files = fileDialog.getAllSelectedFiles();
    int filterIndex = fileDialog.getSelectedFilterIndex();
    switch (filterIndex)
    {
    case SupportedFilesFilterIndex: {
      auto newSources = Superclass::loadFilesForSupportedTypes(files);
      for (auto const& source : newSources)
      {
        sources << source;
      }
    }
    break;
    case AllFilesFilterIndex: {
      auto newSources =
          Superclass::loadFilesForAllTypes(files, server, readerFactory);
      for (auto const& source : newSources)
      {
        sources << source;
      }
    }
    break;
    default:
      // Specific reader
      pqPipelineSource* source = Superclass::loadData(
          files, QString::fromStdString(filtersDetailed[filterIndex].Group),
          QString::fromStdString(filtersDetailed[filterIndex].Name));
      if (source)
      {
        sources << source;
      }
    }
  }
  return sources;
}
