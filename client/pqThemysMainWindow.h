#ifndef pqThemysMainWindow_h
#define pqThemysMainWindow_h

#include <QMainWindow>

/**
 * Themys main window class. Setup GUI and specific actions/reactions.
 */
class pqThemysMainWindow : public QMainWindow
{
  Q_OBJECT
  typedef QMainWindow Superclass;

public:
  pqThemysMainWindow();
  ~pqThemysMainWindow() override;

public slots:
  /**
   * Slot called when the interface mode has changed. Update menus, widgets and
   * toolbars according to the new setting.
   */
  void updateInterfaceMode(int use);

protected:
  /**
   * Hide/Show menus according to the interface mode parameter.
   */
  void setupMenus(int mode);

  //@{
  /**
   * Custom build function used to replace some (re)actions by custom ones.
   */
  void buildFileMenu(QMenu* menu);
  void buildToolbars();
  //@}

  /**
   * Hide/Show toolbars according to the parameter.
   */
  void setupToolbars(int mode);

  /**
   * Hide dock widgets depending on the interface mode setting.
   */
  void setupWidgets(int mode);

protected slots:

  /**
   * Show the help
   */
  void showHelpForProxy(const QString& groupname, const QString& proxyname);

private:
  Q_DISABLE_COPY(pqThemysMainWindow)

  void setupObjectInspector();

  /**
   * Remove the given action (if any with a corresponding name was found) by the
   * open assistant action.
   */
  void replaceByAssistantAction(QWidget* parent, const char* actionName);

  class pqInternals;
  pqInternals* Internals;
};

#endif
