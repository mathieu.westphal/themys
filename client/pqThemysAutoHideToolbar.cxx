#include "pqThemysAutoHideToolbar.h"

#include <QActionEvent>

//-----------------------------------------------------------------------------
pqThemysAutoHideToolbar::pqThemysAutoHideToolbar(const QString& title,
                                                 QWidget* parentW)
    : Superclass(title, parentW)
{
}

//-----------------------------------------------------------------------------
void pqThemysAutoHideToolbar::actionEvent(QActionEvent* event)
{
  this->Superclass::actionEvent(event);
  switch (event->type())
  {
  case QActionEvent::ActionRemoved: {
    if (this->actions().empty())
    {
      this->setVisible(false);
    }
    break;
  };
  case QActionEvent::ActionAdded: {
    if (this->actions().size() == 1)
    {
      this->setVisible(true);
    }
    break;
  }
  default:
    break;
  }
}
