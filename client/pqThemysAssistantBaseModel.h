#ifndef pqThemysAssistantBaseModel_h
#define pqThemysAssistantBaseModel_h

#include <QAbstractItemModel>

#include "pqThemysAssistantBaseManager.h"

class QSortFilterProxyModel;

enum class BaseDisplayState { FILTERED_BASE = 0, RECENT_BASE, STUDY };

/**
 * Model for representing the current available bases for Themys Assistant.
 * This is the model used to fill the file view of the open file dialog.
 */
class pqThemysAssistantBaseModel : public QAbstractItemModel
{
  typedef QAbstractItemModel Superclass;
  Q_OBJECT;

public:
  pqThemysAssistantBaseModel(QObject* parent = nullptr);

  virtual ~pqThemysAssistantBaseModel() = default;

  bool setData(const QModelIndex& idx, const QVariant& value,
               int role) override;
  QVariant data(const QModelIndex& idx, int role) const override;

  /**
   * Defines the available columns for a tree view.
   */
  QVariant headerData(int section, Qt::Orientation, int role) const override;

  //@{
  /**
   * Pure virtual function to override from Superclass
   */
  int columnCount(const QModelIndex&) const override;
  QModelIndex index(int row, int column, const QModelIndex&) const override;
  QModelIndex parent(const QModelIndex&) const override;
  int rowCount(const QModelIndex&) const override;
  bool hasChildren(const QModelIndex& idx) const override;
  Qt::ItemFlags flags(const QModelIndex& idx) const override;
  //@}

  /**
   * Proxy model used for things such as sorting, filtering, etc.
   */
  QSortFilterProxyModel* proxyModel() const { return this->ProxyModel; }

  BaseDisplayState currentState() const { return this->CurrentState; }

  assistant::Base baseAt(int idx) const { return this->Bases[idx]; }

Q_SIGNALS:
  /**
   * Fired when a base get renamed while belonging to a study.
   * This event will be processed so the base is effectively renamed in the
   * study.
   */
  void studyBaseNameChanged(int idx, const QString& newName);

public Q_SLOTS:
  void setBases(const QVector<assistant::Base>& bases, BaseDisplayState state);

private:
  QVector<assistant::Base> Bases;
  QSortFilterProxyModel* const ProxyModel;
  BaseDisplayState CurrentState = BaseDisplayState::FILTERED_BASE;
};

#endif // pqThemysAssistantBaseModel_h
