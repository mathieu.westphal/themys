#include "pqThemysInterfaceModeToolbar.h"

#include <QApplication>
#include <QComboBox>
#include <QLabel>
#include <QMessageBox>
#include <QStyle>

#include <pqActiveObjects.h>
#include <pqApplicationCore.h>
#include <pqCoreUtilities.h>
#include <pqPluginManager.h>
#include <pqServerManagerModel.h>
#include <pqSettings.h>
#include <pqUndoStack.h>
#include <vtkCommand.h>
#include <vtkSMProperty.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMProxyManager.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMSettings.h>
#include <vtkSMSettingsProxy.h>

#include "pqThemysSettingsInfo.h"

//-----------------------------------------------------------------------------
class pqThemysInterfaceModeToolbar::vtkInternalObserver : public vtkCommand
{
public:
  static vtkInternalObserver* New() { return new vtkInternalObserver(); }

  void Execute(vtkObject* /*caller*/, unsigned long /*eventId*/, void* callData)
  {
    vtkSMProxyManager::RegisteredProxyInformation* info =
        static_cast<vtkSMProxyManager::RegisteredProxyInformation*>(callData);

    if (info && info->GroupName && info->ProxyName &&
        strcmp(info->GroupName, "settings") == 0 &&
        strcmp(info->ProxyName, SETTINGS_GROUP_NAME.toUtf8().data()) == 0)
    {
      self->setupConnections(vtkSMSettingsProxy::SafeDownCast(info->Proxy));
    }
  }

  pqThemysInterfaceModeToolbar* self = nullptr;
};

//-----------------------------------------------------------------------------
pqThemysInterfaceModeToolbar::pqThemysInterfaceModeToolbar(
    nlohmann::json& interfaces, QWidget* parentW)
    : Superclass(parentW)
{
  this->setWindowTitle("InterfaceMode");
  this->constructor(interfaces);
}

//-----------------------------------------------------------------------------
void pqThemysInterfaceModeToolbar::constructor(nlohmann::json& interfaces)
{
  this->InterfaceOptions = new QComboBox(this);
  for (nlohmann::json& mode : interfaces)
  {
    this->InterfaceOptions->addItem(
        QString(mode["name"].template get<std::string>().c_str()), 0);
  }

  this->addWidget(InterfaceOptions);

  // Every time the server is changed we add an observer to keep track of when
  // the setting proxy is added. When it is, the observer will call
  // `setupConnections` so everything is in order for the GUI. We use the
  // `preServerAdded` because code path is
  //  - fire pqServerManagerModel::preServerAdded event
  //  - create all necessary proxies, including the pre-loaded plugins proxies
  //  - fire pqServerManagerModel::serverAdded and pqActiveObject::serverChanged
  //  events
  QObject::connect(
      pqApplicationCore::instance()->getServerManagerModel(),
      &pqServerManagerModel::preServerAdded, [this](pqServer* server) {
        if (server)
        {
          vtkNew<pqThemysInterfaceModeToolbar::vtkInternalObserver> observer;
          observer->self = this;

          // reset internal proxy so we do no try to call function on the proxy
          // of the former server
          this->SettingsProxy = nullptr;
          server->proxyManager()->AddObserver(vtkCommand::RegisterEvent,
                                              observer);
        }
      });
}

//----------------------------------------------------------------------------
void pqThemysInterfaceModeToolbar::setupConnections(
    vtkSMSettingsProxy* settingsProxy)
{
  if (settingsProxy != nullptr && settingsProxy != this->SettingsProxy)
  {
    this->SettingsProxy = settingsProxy;

    // Connect on the property
    pqCoreUtilities::connect(
        this->SettingsProxy->GetProperty(INTERFACE_MODE_NAME.toUtf8().data()),
        vtkCommand::ModifiedEvent, this, SLOT(updateInterfaceMode()));

    // Connect the combobox
    this->connect(this->InterfaceOptions, SIGNAL(currentIndexChanged(int)),
                  SLOT(onTriggered(int)));

    // Init the combobox state
    this->updateInterfaceMode();
  }
}

//-----------------------------------------------------------------------------
void pqThemysInterfaceModeToolbar::onTriggered(int status)
{
  SCOPED_UNDO_EXCLUDE();
  if (!this->SettingsProxy)
  {
    return;
  }

  // Set the property
  vtkSMPropertyHelper(this->SettingsProxy, INTERFACE_MODE_NAME.toUtf8().data())
      .Set(status);
  this->SettingsProxy->UpdateVTKObjects();

  // Manually save it to user settings files
  pqSettings* qSettings = pqApplicationCore::instance()->settings();
  qSettings->saveInQSettings(
      QString(SETTINGS_GROUP_NAME + "." + INTERFACE_MODE_NAME).toUtf8().data(),
      this->SettingsProxy->GetProperty(INTERFACE_MODE_NAME.toUtf8().data()));
  vtkSMSettings* settings = vtkSMSettings::GetInstance();
  settings->SetSetting(
      QString(".settings." + SETTINGS_GROUP_NAME + "." + INTERFACE_MODE_NAME)
          .toUtf8()
          .data(),
      status);
}

//-----------------------------------------------------------------------------
void pqThemysInterfaceModeToolbar::updateInterfaceMode()
{
  if (!this->SettingsProxy)
  {
    return;
  }

  // Recover the property value
  int mode = vtkSMPropertyHelper(this->SettingsProxy,
                                 INTERFACE_MODE_NAME.toUtf8().data())
                 .GetAsInt();

  // Update the combobox status
  this->InterfaceOptions->setCurrentIndex(mode);

  // Inform the application the setting has changed
  emit changeInterfaceMode(mode);
}
