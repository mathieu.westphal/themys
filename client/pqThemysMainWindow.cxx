#include "pqThemysMainWindow.h"

#include <filesystem>

#include <pqApplicationCore.h>
#include <pqCoreUtilities.h>
#include <pqDeleteReaction.h>
#include <pqHelpReaction.h>
#include <pqInterfaceTracker.h>
#include <pqMacroReaction.h>
#include <pqPVApplicationCore.h>
#include <pqParaViewBehaviors.h>
#include <pqParaViewMenuBuilders.h>
#include <pqPythonManager.h>
#include <pqPythonShell.h>
#include <pqRepresentation.h>
#include <pqServerManagerModel.h>
#include <vtkNew.h>
#include <vtkPVFileInformation.h>
#include <vtkPVGeneralSettings.h>
#include <vtkPVLogger.h>
#include <vtkResourceFileLocator.h>
#include <vtkSMProperty.h>
#include <vtkSMProxy.h>
#include <vtkSMSettings.h>
#include <vtk_nlohmannjson.h>
#include <vtksys/FStream.hxx>

#include "ui_pqThemysMainWindow.h"
#include VTK_NLOHMANN_JSON(json.hpp)

#include <QAction>
#include <QDebug>
#include <QSet>

#include "pqDesktopServicesReaction.h"
#include "pqLoadDataReaction.h"
#include "pqThemysAssistantReaction.h"
#include "pqThemysAutoHideToolbar.h"
#include "pqThemysDocumentationLocation.h"
#include "pqThemysInterfaceModeToolbar.h"
#include "pqThemysSettingsReaction.h"
#include "pqThemysViewFrameActionsImplementation.h"

static const QString THEMYS_DOC_SITE = "Site web";
static const QString TOOLBARS_SUBMENU = "Toolbars";
static const std::string INTERFACE_CONFIGURATION_FILENAME =
    "interface_modes.json";

namespace {
/**
 * Returns a true if the given json string list contains an item that matches
 * exactly the second argument.
 */
bool jsonListContains(const nlohmann::json& stringList,
                      const QString& compareString)
{
  return std::any_of(stringList.begin(), stringList.end(),
                     [compareString](nlohmann::json element) {
                       return element.template get<std::string>() ==
                              compareString.toStdString();
                     });
}

/**
 * Show items of a list of actions if they can be found in a json string list.
 * All items are shown if the json list is empty.
 */
void showMenuActions(const nlohmann::json& filter, QMenu* menu)
{
  // Show needed menu actions
  for (auto action : menu->actions())
  {
    action->setVisible(filter.is_null() ||
                       ::jsonListContains(filter, action->text()));
  }

  // Show all submenu actions, that could be hidden previously by partial menus.
  for (QMenu* subMenu : menu->findChildren<QMenu*>())
  {
    for (auto action : subMenu->actions())
    {
      action->setVisible(true);
    }
  }
}

/**
 * Only show subitems of a menu specified by a json object, and hide the others.
 * `partialMenu` should be a list of json objects containing a "name" property,
 * the name of the submenu, and a string list "show" of the items to be shown.
 */
void showPartialMenu(const nlohmann::json& partialMenu, QMenu* menu)
{
  for (nlohmann::json sourcesCategory : partialMenu)
  {
    QString categoryName =
        QString(sourcesCategory["name"].template get<std::string>().c_str());
    if (categoryName.startsWith("&"))
    {
      // Remove the "&" prefix if any; QMenu names do not have a leading "&", as
      // opposed to actions.
      categoryName = categoryName.mid(1);
    }

    QMenu* subMenu =
        menu->findChild<QMenu*>(categoryName, Qt::FindDirectChildrenOnly);
    if (!subMenu)
    {
      continue;
    }

    // Show the submenu itself
    for (auto action : menu->actions())
    {
      if (action->text().toStdString() ==
          sourcesCategory["name"].template get<std::string>())
      {
        action->setVisible(true);
      }
    }

    // On show specified actions
    for (auto action : subMenu->actions())
    {
      action->setVisible(
          ::jsonListContains(sourcesCategory["show"], action->text()));
    }
  }
}

/**
 * Return the path to the interface configuration json file, located in the
 * share/ folder, found relatively to the path of the application executable
 * being run.
 */
std::string getInterfaceConfigFileName()
{
  std::string interfaceConfigFileName;
  std::string appLocation =
      pqCoreUtilities::getParaViewApplicationDirectory().toStdString();
  std::vector<std::string> prefixes{"share"};
  vtkNew<vtkResourceFileLocator> locator;
  std::string path =
      locator->Locate(appLocation, prefixes, INTERFACE_CONFIGURATION_FILENAME);
  return vtksys::SystemTools::CollapseFullPath(INTERFACE_CONFIGURATION_FILENAME,
                                               path)
      .c_str();
}
} // namespace

//-----------------------------------------------------------------------------
class pqThemysMainWindow::pqInternals : public Ui::pqClientMainWindow
{
public:
  int InterfaceMode = 0;
  nlohmann::json InterfaceConfig;

  /**
   * Fill a given menu with actions whose names are specified by a json string
   * list
   */
  void fillThemysMenu(QMainWindow* window, QMenu* menu,
                      const nlohmann::json& actions)
  {
    for (const nlohmann::json& actionJson : actions)
    {
      QString actionName{actionJson.template get<std::string>().c_str()};
      if (actionName == "separator")
      {
        menu->addSeparator();
      } else if (auto action = window->findChild<QAction*>(actionName))
      {
        menu->addAction(action);
      } else if (QMenu* submenu = window->findChild<QMenu*>(actionName))
      {
        menu->addMenu(submenu);
      } else
      {
        qWarning() << "Menu entry <" << actionName.toLatin1().data()
                   << "> not found.";
      }
    }
  }
};

//-----------------------------------------------------------------------------
pqThemysMainWindow::pqThemysMainWindow()
{
  // Setup default GUI layout.
  this->Internals = new pqInternals();
  this->Internals->setupUi(this);

  // Setup the python shell
  pqPythonShell* shell = new pqPythonShell(this);
  shell->setObjectName("pythonShell");
  this->Internals->pythonShellDock->setWidget(shell);

  // Setup the dock window corners to give the vertical docks more room.
  this->setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
  this->setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

  // Load JSON interface configuration file
  vtksys::ifstream interfaceConfigFile(::getInterfaceConfigFileName());
  nlohmann::json jsonConfig = nlohmann::json::parse(interfaceConfigFile);
  nlohmann::json customMenus = jsonConfig["custom_menus"];
  this->Internals->InterfaceConfig = jsonConfig["interfaces"];

  // Populate file menu.
  this->buildFileMenu(this->Internals->menuFile);
  // Create a custom base file menu
  this->Internals->fillThemysMenu(this, this->Internals->menuFileBase,
                                  customMenus["file"]);

  // Populate edit menu.
  pqParaViewMenuBuilders::buildEditMenu(*this->Internals->menuEdit,
                                        this->Internals->propertiesPanel);
  // Create a custom base edit menu
  this->Internals->fillThemysMenu(this, this->Internals->menuEditBase,
                                  customMenus["edit"]);
  new pqThemysSettingsReaction(this->Internals->actionSettings);

  // Populate sources menu.
  pqParaViewMenuBuilders::buildSourcesMenu(*this->Internals->menuSources, this);
  QObject::connect(this->Internals->menuSources, &QMenu::aboutToShow,
                   [&]() { this->setupMenus(this->Internals->InterfaceMode); });

  // Populate filters menu.
  pqParaViewMenuBuilders::buildFiltersMenu(*this->Internals->menuFilters, this);
  QObject::connect(this->Internals->menuFilters, &QMenu::aboutToShow,
                   [&]() { this->setupMenus(this->Internals->InterfaceMode); });

  // Populate View menu.
  pqParaViewMenuBuilders::buildViewMenu(*this->Internals->menuView, *this);
  QObject::connect(this->Internals->menuView, &QMenu::aboutToShow,
                   [&]() { this->setupMenus(this->Internals->InterfaceMode); });

  // Populate Tools menu.
  pqParaViewMenuBuilders::buildToolsMenu(*this->Internals->menuTools);

  // Setup the menu to show macros.
  pqParaViewMenuBuilders::buildMacrosMenu(*this->Internals->menuMacros);

  // Create a custom base tools menu, containing macro menu
  this->Internals->fillThemysMenu(this, this->Internals->menuToolsBase,
                                  customMenus["tools"]);

  // Setup the help menu.
  pqParaViewMenuBuilders::buildHelpMenu(*this->Internals->menuHelp);

  // Create a custom base help menu. Rename the documentation menu entry.
  this->Internals->fillThemysMenu(this, this->Internals->menuHelpBase,
                                  customMenus["help"]);
  QAction* site = this->Internals->menuHelp->findChild<QAction*>("actionGuide");
  site->setText(THEMYS_DOC_SITE);
  // The pqDesktopServicesReaction class creates a connection between itself and
  // the site object in its ctor. Remove the old one and replaces it.
  site->disconnect(SIGNAL(triggered(bool)));

  // WARNING : linux only way of getting the path to current exe
  const auto& bin_path =
      std::filesystem::canonical("/proc/self/exe").parent_path();
  const auto doc_abs_path{bin_path / THEMYS_DOCUMENTATION_RELATIVE_LOCATION};
  QString siteURL(doc_abs_path.c_str());
  new pqDesktopServicesReaction(QUrl(siteURL), site);

  // One time widget actions
  // Setup color editor
  // Provide access to the color-editor panel for the application and hide it
  pqApplicationCore::instance()->registerManager(
      "COLOR_EDITOR_PANEL", this->Internals->colorMapEditorDock);

  // Provide access to the find data panel for the application.
  pqApplicationCore::instance()->registerManager("FIND_DATA_PANEL",
                                                 this->Internals->findDataDock);

  // Setup default GUI layout.
  this->setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::North);

  // Set up the dock window corners to give the vertical docks more room.
  this->setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
  this->setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);

  this->tabifyDockWidget(this->Internals->colorMapEditorDock,
                         this->Internals->memoryInspectorDock);
  this->tabifyDockWidget(this->Internals->colorMapEditorDock,
                         this->Internals->findDataDock);
  this->tabifyDockWidget(this->Internals->colorMapEditorDock,
                         this->Internals->timeManagerDock);
  this->tabifyDockWidget(this->Internals->colorMapEditorDock,
                         this->Internals->comparativePanelDock);
  this->tabifyDockWidget(this->Internals->colorMapEditorDock,
                         this->Internals->collaborationPanelDock);
  this->tabifyDockWidget(this->Internals->colorMapEditorDock,
                         this->Internals->lightInspectorDock);

  // Hide all other dockwidgets by default
  this->Internals->animationViewDock->hide();
  this->Internals->colorMapEditorDock->hide();
  this->Internals->findDataDock->hide();
  this->Internals->memoryInspectorDock->hide();
  this->Internals->multiBlockInspectorDock->hide();
  this->Internals->outputWidgetDock->hide();
  this->Internals->pythonShellDock->hide();
  this->Internals->statisticsDock->hide();
  this->Internals->comparativePanelDock->hide();
  this->Internals->collaborationPanelDock->hide();
  this->Internals->timeManagerDock->hide();
  this->Internals->lightInspectorDock->hide();

  this->tabifyDockWidget(this->Internals->animationViewDock,
                         this->Internals->statisticsDock);
  this->tabifyDockWidget(this->Internals->animationViewDock,
                         this->Internals->outputWidgetDock);
  this->tabifyDockWidget(this->Internals->animationViewDock,
                         this->Internals->pythonShellDock);
  this->setupObjectInspector();

  // Setup the context menu for the pipeline browser.
  pqParaViewMenuBuilders::buildPipelineBrowserContextMenu(
      *this->Internals->pipelineBrowser->contextMenu());

  this->buildToolbars();

  // Interface mode
  pqThemysInterfaceModeToolbar* interfaceModeToolbar =
      new pqThemysInterfaceModeToolbar(this->Internals->InterfaceConfig, this);
  interfaceModeToolbar->setObjectName("InterfaceMode");
  interfaceModeToolbar->layout()->setSpacing(0);
  this->addToolBar(interfaceModeToolbar);
  this->connect(interfaceModeToolbar, SIGNAL(changeInterfaceMode(int)),
                SLOT(updateInterfaceMode(int)));

  // remove PV macro toolbar and use our custom one.
  QToolBar* pvMacros = this->findChild<QToolBar*>("MacrosToolbar");
  auto themysMacros =
      new pqThemysAutoHideToolbar("Themys Macros Toolbar", this);
  themysMacros->setObjectName("ThemysMacrosToolbar");
  themysMacros->setVisible(false);
  this->insertToolBar(pvMacros, themysMacros);
  this->removeToolBar(pvMacros);
  pvMacros->deleteLater();

  pqPythonManager* manager = pqPVApplicationCore::instance()->pythonManager();
  if (manager)
  {
    manager->addWidgetForRunMacros(this->Internals->menuToolsBase);
    manager->addWidgetForRunMacros(themysMacros);
    manager->addWidgetForEditMacros(this->Internals->menuToolsEditMacro);
    manager->addWidgetForDeleteMacros(this->Internals->menuToolsDeleteMacro);
  }

  // Enable help from the properties panel.
  QObject::connect(this->Internals->propertiesPanel,
                   SIGNAL(helpRequested(const QString&, const QString&)), this,
                   SLOT(showHelpForProxy(const QString&, const QString&)));

  /// Hook delete to pqDeleteReaction.
  QAction* tempDeleteAction = new QAction(this);
  pqDeleteReaction* handler = new pqDeleteReaction(tempDeleteAction);
  handler->connect(this->Internals->propertiesPanel,
                   SIGNAL(deleteRequested(pqProxy*)),
                   SLOT(deleteSource(pqProxy*)));

  // Define application behaviors.
  pqParaViewBehaviors::setEnableStandardViewFrameActions(false);
  pqInterfaceTracker* pgm = pqApplicationCore::instance()->interfaceTracker();
  pgm->addInterface(new pqThemysViewFrameActionsImplementation(pgm));

  new pqParaViewBehaviors(this, this);

  // Change interface for every representation
  QObject::connect(
      pqApplicationCore::instance()->getServerManagerModel(),
      &pqServerManagerModel::representationAdded, [](pqRepresentation* repr) {
        vtkSMProxy* proxy = repr->getProxy();
        if (proxy && strcmp(proxy->GetXMLGroup(), "representations") == 0)
        {
          vtkSMProperty* polarProp = proxy->GetProperty("PolarAxes");
          if (polarProp)
          {
            polarProp->SetPanelVisibility("default");
          }
          vtkSMProperty* specularProp = proxy->GetProperty("Specular");
          if (specularProp)
          {
            specularProp->SetPanelVisibility("advanced");
          }
        }
      });
}

//-----------------------------------------------------------------------------
pqThemysMainWindow::~pqThemysMainWindow() { delete this->Internals; }

//-----------------------------------------------------------------------------
void pqThemysMainWindow::updateInterfaceMode(int mode)
{
  this->setupMenus(mode);
  this->setupWidgets(mode);
  this->setupToolbars(mode);
  this->Internals->InterfaceMode = mode;
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::setupMenus(int mode)
{
  // Show/hide custom menus, containing fewer items
  bool useCustomMenus =
      this->Internals->InterfaceConfig[mode]["use_custom_menus"];
  this->Internals->menuFileBase->menuAction()->setVisible(useCustomMenus);
  this->Internals->menuEditBase->menuAction()->setVisible(useCustomMenus);
  this->Internals->menuToolsBase->menuAction()->setVisible(useCustomMenus);
  this->Internals->menuHelpBase->menuAction()->setVisible(useCustomMenus);
  this->Internals->menuEdit->menuAction()->setVisible(!useCustomMenus);
  this->Internals->menuFile->menuAction()->setVisible(!useCustomMenus);
  this->Internals->menuTools->menuAction()->setVisible(!useCustomMenus);
  this->Internals->menuMacros->menuAction()->setVisible(!useCustomMenus);
  this->Internals->menuHelp->menuAction()->setVisible(!useCustomMenus);

  // Filter sources to only show the ones in by `sources` if specified
  nlohmann::json sourcesFilter =
      this->Internals->InterfaceConfig[mode]["sources"];
  nlohmann::json partialSources =
      this->Internals->InterfaceConfig[mode]["partial_sources"];
  ::showMenuActions(sourcesFilter, this->Internals->menuSources);
  ::showPartialMenu(partialSources, this->Internals->menuSources);

  // Only show filter categories present in the `filters` array
  nlohmann::json filtersFilter =
      this->Internals->InterfaceConfig[mode]["filters"];
  nlohmann::json partialFilters =
      this->Internals->InterfaceConfig[mode]["partial_filters"];
  ::showMenuActions(filtersFilter, this->Internals->menuFilters);
  ::showPartialMenu(partialFilters, this->Internals->menuFilters);

  // Don't show the hidden toolbars in the toolbar selection menu
  QMenu* toolbarsMenu = this->Internals->menuView->findChild<QMenu*>(
      TOOLBARS_SUBMENU, Qt::FindDirectChildrenOnly);
  nlohmann::json hiddenWidgets =
      this->Internals->InterfaceConfig[mode]["hidden_widgets"];
  for (auto action : toolbarsMenu->actions())
  {
    action->setVisible(!::jsonListContains(hiddenWidgets, action->text()));
  }
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::buildFileMenu(QMenu* menu)
{
  pqParaViewMenuBuilders::buildFileMenu(*menu);

  this->replaceByAssistantAction(menu, "actionFileOpen");
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::buildToolbars()
{
  pqParaViewMenuBuilders::buildToolbars(*this);
  this->addToolBarBreak();

  QToolBar* mainToolbar = this->findChild<QToolBar*>("MainControlsToolbar");
  this->replaceByAssistantAction(mainToolbar, "actionOpenData");
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::replaceByAssistantAction(QWidget* parent,
                                                  const char* actionName)
{
  if (!parent)
  {
    return;
  }

  if (QAction* openData = parent->findChild<QAction*>(actionName))
  {
    // Add custom open assistant action
    QAction* openAssistant = new QAction(parent);
    openAssistant->setText(openData->text());
    openAssistant->setIcon(openData->icon());
    openAssistant->setShortcut(openData->shortcut());
    openAssistant->setShortcutContext(openData->shortcutContext());
    openAssistant->setObjectName(actionName);
    parent->insertAction(openData, openAssistant);
    new pqThemysAssistantReaction(openAssistant);

    // Remove former one
    parent->removeAction(openData);
    this->removeAction(openData);
    delete openData;
  }
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::setupWidgets(int mode)
{
  nlohmann::json hiddenWidgets =
      this->Internals->InterfaceConfig[mode]["hidden_widgets"];

  QList<QDockWidget*> docks = this->findChildren<QDockWidget*>();
  for (auto dock : docks)
  {
    if (::jsonListContains(hiddenWidgets, dock->windowTitle()))
    {
      dock->hide();
    }
  }
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::setupToolbars(int mode)
{
  nlohmann::json hiddenWidgets =
      this->Internals->InterfaceConfig[mode]["hidden_widgets"];
  nlohmann::json previouslyHiddenWidgets =
      this->Internals
          ->InterfaceConfig[this->Internals->InterfaceMode]["hidden_widgets"];
  nlohmann::json partialToolbars =
      this->Internals->InterfaceConfig[mode]["partial_toolbars"];

  QList<QToolBar*> toolbars = this->findChildren<QToolBar*>();
  for (auto toolbar : toolbars)
  {
    for (auto action : toolbar->actions())
    {
      action->setVisible(true); // Reset visibility of all actions
    }

    if (::jsonListContains(previouslyHiddenWidgets, toolbar->windowTitle()))
    {
      toolbar->setVisible(true); // Show again the toolbars that were hidden in
                                 // the previous mode
    }

    if (::jsonListContains(hiddenWidgets, toolbar->windowTitle()))
    {
      toolbar->setVisible(false);
    }

    // Hide items from some toolbars
    for (nlohmann::json partialToolbar : partialToolbars)
    {
      if (partialToolbar["name"].template get<std::string>() ==
          toolbar->objectName().toStdString())
      {
        for (auto action : toolbar->actions())
        {
          action->setVisible(
              ::jsonListContains(partialToolbar["show"], action->objectName()));
        }
      }
    }
  }
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::showHelpForProxy(const QString& groupname,
                                          const QString& proxyname)
{
#ifdef PARAVIEW_USE_QTHELP
  pqHelpReaction::showProxyHelp(groupname, proxyname);
#else
  Q_UNUSED(groupname)
  Q_UNUSED(proxyname)
#endif
}

//-----------------------------------------------------------------------------
void pqThemysMainWindow::setupObjectInspector()
{
  this->tabifyDockWidget(this->Internals->propertiesDock,
                         this->Internals->viewPropertiesDock);
  this->tabifyDockWidget(this->Internals->propertiesDock,
                         this->Internals->displayPropertiesDock);
  this->tabifyDockWidget(this->Internals->propertiesDock,
                         this->Internals->informationDock);
  this->tabifyDockWidget(this->Internals->propertiesDock,
                         this->Internals->multiBlockInspectorDock);

  int propertiesPanelMode = vtkSMSettings::GetInstance()->GetSettingAsInt(
      ".settings.GeneralSettings.PropertiesPanelMode",
      vtkPVGeneralSettings::ALL_IN_ONE);

  switch (propertiesPanelMode)
  {
  case vtkPVGeneralSettings::SEPARATE_DISPLAY_PROPERTIES:
    delete this->Internals->viewPropertiesPanel;
    delete this->Internals->viewPropertiesDock;
    this->Internals->viewPropertiesPanel = nullptr;
    this->Internals->viewPropertiesDock = nullptr;

    this->Internals->propertiesPanel->setPanelMode(
        pqPropertiesPanel::SOURCE_PROPERTIES |
        pqPropertiesPanel::VIEW_PROPERTIES);
    break;

  case vtkPVGeneralSettings::SEPARATE_VIEW_PROPERTIES:
    delete this->Internals->displayPropertiesPanel;
    delete this->Internals->displayPropertiesDock;
    this->Internals->displayPropertiesPanel = nullptr;
    this->Internals->displayPropertiesDock = nullptr;

    this->Internals->propertiesPanel->setPanelMode(
        pqPropertiesPanel::SOURCE_PROPERTIES |
        pqPropertiesPanel::DISPLAY_PROPERTIES);
    break;

  case vtkPVGeneralSettings::ALL_SEPARATE:
    this->Internals->propertiesPanel->setPanelMode(
        pqPropertiesPanel::SOURCE_PROPERTIES);
    break;

  case vtkPVGeneralSettings::ALL_IN_ONE:
  default:
    delete this->Internals->viewPropertiesPanel;
    delete this->Internals->viewPropertiesDock;
    this->Internals->viewPropertiesPanel = nullptr;
    this->Internals->viewPropertiesDock = nullptr;

    delete this->Internals->displayPropertiesPanel;
    delete this->Internals->displayPropertiesDock;
    this->Internals->displayPropertiesPanel = nullptr;
    this->Internals->displayPropertiesDock = nullptr;
    break;
  }

  this->Internals->propertiesDock->show();
  this->Internals->propertiesDock->raise();
}
