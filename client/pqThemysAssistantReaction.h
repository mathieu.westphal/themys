#ifndef pqThemysAssistantReaction_h
#define pqThemysAssistantReaction_h

#include "pqLoadDataReaction.h"

/**
 * @ingroup Reactions
 * Reaction for opening data files. The only difference with pqLoadDataReaction
 * is that it uses pqThemysAssistantDialog instead of the Paraview file dialog.
 */
class pqThemysAssistantReaction : public pqLoadDataReaction
{
  Q_OBJECT
  typedef pqLoadDataReaction Superclass;

public:
  /**
   * Constructor. Parent cannot be nullptr.
   */
  pqThemysAssistantReaction(QAction* parent);

  /**
   * Create a dialog to select data.
   * Load selected data.
   * Optionally rename pipeline source according to a "PipelineName" string
   * property of the reader.
   */
  static QList<pqPipelineSource*> loadData();

protected Q_SLOTS:
  /**
   * Called when the action is triggered.
   */
  void onTriggered() override
  {
    QList<pqPipelineSource*> sources = pqThemysAssistantReaction::loadData();
    pqPipelineSource* source;
    Q_FOREACH (source, sources)
    {
      Q_EMIT this->loadedData(source);
    }
  }

private:
  Q_DISABLE_COPY(pqThemysAssistantReaction)
};

#endif
