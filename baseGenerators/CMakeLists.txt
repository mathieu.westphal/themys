cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

project(HyperTreeGridBase)

find_package(
  VTK
  COMPONENTS vtkCommonCore vtkCommonDataModel vtkFiltersGeneral
             vtkFiltersHyperTree vtkFiltersSources vtkIOXML
  QUIET)

if(NOT VTK_FOUND)
  message("Skipping HyperTreeGridBase: ${VTK_NOT_FOUND_MESSAGE}")
  return()
endif()
message(STATUS "VTK_VERSION: ${VTK_VERSION}")
if(VTK_VERSION VERSION_LESS "8.90.0")
  # old system
  include(${VTK_USE_FILE})
  add_executable(HyperTreeGridBase MACOSX_BUNDLE HyperTreeGridBase.cxx)
  target_link_libraries(HyperTreeGridBase PRIVATE ${VTK_LIBRARIES})
else()
  # include all components
  add_executable(HyperTreeGridBase MACOSX_BUNDLE HyperTreeGridBase.cxx)
  target_link_libraries(HyperTreeGridBase PRIVATE ${VTK_LIBRARIES})
  # vtk_module_autoinit is needed
  vtk_module_autoinit(TARGETS HyperTreeGridBase MODULES ${VTK_LIBRARIES})
endif()
