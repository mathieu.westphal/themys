from BaseBuilder import *

import math
import os
import sys
import vtk

absCellIndex = 0


# -----------------------------------------------------------------------------
# Inpired from https://kitware.github.io/vtk-examples/site/Python/StructuredGrid/SGrid/
def BuildDomain(timeIdx, matIdx, domainIdx, isTwin):
    lenDomains = len(domains)
    suffixTwin = "_Twin" if isTwin else ""

    # The whole dataset will be a 30x20 [-1;1] square along the XY plane
    dims = [5, 5, 1]
    pMin = [-1 + (2 / lenDomains) * domainIdx, -1 + (2 / len(materials)) * matIdx]
    pMax = [
        -1 + (2 / lenDomains) * (domainIdx + 1),
        -1 + (2 / len(materials)) * (matIdx + 1),
    ]

    # Create the structured grid.
    sgrid = vtk.vtkStructuredGrid()
    sgrid.SetDimensions(dims)

    nPoints = dims[0] * dims[1]

    # We also create the points and vectors.
    vectors = vtk.vtkDoubleArray()
    vectors.SetNumberOfComponents(3)
    vectors.SetNumberOfTuples(nPoints)
    vectors.SetName("VectorData" + suffixTwin)
    scalars = vtk.vtkDoubleArray()
    scalars.SetNumberOfComponents(1)
    scalars.SetNumberOfTuples(nPoints)
    scalars.SetName("ScalarData" + suffixTwin)
    strings = vtk.vtkStringArray()
    strings.SetNumberOfTuples(nPoints)
    strings.SetName("StringData" + suffixTwin)
    points = vtk.vtkPoints()
    points.Allocate(nPoints)

    sizeCellX = 2 / (len(domains) * (dims[0] - 1))
    sizeCellY = 2 / (len(materials) * (dims[1] - 1))

    pos = [0.0] * 3
    for j in range(0, dims[1]):
        pos[1] = pMin[1] + j * sizeCellY
        for i in range(0, dims[0]):
            pos[0] = pMin[0] + i * sizeCellX
            idx = points.InsertNextPoint(pos)

            # some data generated from points
            scalVec = [a - b for a, b in zip(pos, [0, 0, -1])]
            length = math.sqrt(sum((a * a for a in scalVec)))
            vectors.InsertTuple(idx, scalVec)
            scalars.SetValue(idx, length * (timeIdx + 1))
            strings.SetValue(idx, "Point " + str(idx))

    sgrid.SetPoints(points)
    sgrid.GetPointData().AddArray(scalars)
    sgrid.GetPointData().AddArray(vectors)
    sgrid.GetPointData().AddArray(strings)

    # Cell data arrays

    qualityFilter = vtk.vtkMeshQuality()
    qualityFilter.SetInputData(sgrid)
    qualityFilter.SaveCellQualityOn()
    qualityFilter.SetQuadQualityMeasureToArea()
    qualityFilter.Update()
    areaArray = qualityFilter.GetOutput().GetCellData().GetArray("Quality")
    # This function BuildDomain generate a fraction of a plane with X&Y E [-1;1]
    # Total area of this cylinder is 4.0, but we compute the cell volume relative to the material
    totalArea = 4.0 / len(materials)
    relativeArea = areaArray.NewInstance()
    relativeArea.SetNumberOfValues(areaArray.GetNumberOfValues())
    relativeArea.SetName("RelativeCellArea" + suffixTwin)
    for i in range(0, areaArray.GetNumberOfValues()):
        relativeArea.SetValue(i, areaArray.GetValue(i) / totalArea)

    cellIndexArray = vtk.vtkIntArray()
    cellIndexArray.SetNumberOfValues(areaArray.GetNumberOfValues())
    cellIndexArray.SetName("CellAbsoluteIndex" + suffixTwin)
    global absCellIndex
    for i in range(0, areaArray.GetNumberOfValues()):
        cellIndexArray.SetValue(i, absCellIndex)
        absCellIndex += 1

    cellNbStrArray = vtk.vtkStringArray()
    cellNbStrArray.SetNumberOfTuples(sgrid.GetNumberOfCells())
    cellNbStrArray.SetName("CellNumber" + suffixTwin)
    for i in range(0, sgrid.GetNumberOfCells()):
        cellNbStrArray.SetValue(i, "Cell " + str(i))

    sgrid.GetCellData().AddArray(relativeArea)
    sgrid.GetCellData().AddArray(cellIndexArray)
    sgrid.GetCellData().AddArray(cellNbStrArray)

    return sgrid


# -----------------------------------------------------------------------------
if __name__ == "__main__":
    args = sys.argv
    if len(args) < 2:
        print("Path to output directory should be specified")
        sys.exit()

    path = args[1]
    if isinstance(path, str) is False or os.path.isdir(path) is False:
        print("Invalid directory:", path)
        sys.exit()

    Build(path, "SG2DBase", "SG 2D Base Example", BuildDomain, False)
    # Reset absolute cell indices
    absCellIndex = 0
    Build(path, "SG2DBase_Twin", "SG 2D Base Example (Twin)", BuildDomain, True)
