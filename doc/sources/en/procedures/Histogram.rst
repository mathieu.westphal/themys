Documentation: `doc fr <../../fr/procedures/Histogram.html>`__



Create a histogram
==================

Description
-----------

A histogram is a type of bar chart which first separates the data array
range into equally sized bins. The plot then shows the number of cells
or points that belong to each bin according to their scalar data value.

Procedure using the GUI
-----------------------

Start by selecting the data set from which to create a histogram in the
``Pipeline Browser`` panel by clicking on it. Then activate the
``Advanced Mode`` by clicking on the corresponding wheel button as shown
below. In the menu, go to ``Filters > Data Analysis > Histogram``.

|image0|

In the ``Properties`` panel, choose the data array to create a histogram
from with the ``Select Input Array`` combo box. Select the number of
bins for the histogram using the ``Bin Count`` slider, or by directly
entering a value. The ``Component`` check box is used to choose a vector
component when the data array is vectorial. Click on ``Apply`` to
produce the histogram, which appears in a new chart view.

|image1|

Procedure using Python scripting
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the data set from which to produce a histogram
   mySource = FindSource('Wavelet1')

   # Create a new 'Histogram' filter
   histogram = Histogram(Input=mySource)

   # If needed, change scalar data array
   # Use 'CELLS' instead of 'POINTS' to use a cell array
   # Replace 'RTData' with a valid scalar array
   histogram.SelectInputArray = ['POINTS', 'RTData']

   # If needed, change the number of bins
   histogram.BinCount = 15

   # Create a bar chart view
   barChartView = CreateView('XYBarChartView')

   # Show output data
   histogramDisplay = Show(histogram, barChartView)

   # Get layout
   layout = GetLayout()

   # Add bar chart view to the layout
   AssignViewToLayout(barChartView, layout)

   # If needed, create a new layout containing only the histogram
   layout2 = CreateLayout(name='Histogram')

   # Create a new 'Bar Chart View'
   barChartView2 = CreateView('XYBarChartView')

   # Show output data
   histogramDisplay2 = Show(histogram, barChartView2)

   # Add bar chart view to new layout
   AssignViewToLayout(barChartView2, layout2)

.. |image0| image:: ../../img/procedures/13HistogramFilter.png
.. |image1| image:: ../../img/procedures/13HistogramResult.png
