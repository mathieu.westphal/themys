Documentation: `doc fr <../../fr/procedures/SelectionByGlobalID.html>`__



Define a selection using global identifiers (IDs)
=================================================

Description
-----------

The selection operation highlights a subset of elements in a data set,
typically cells or points, which can then be extracted or analyzed. In
particular, selection with global identifiers (IDs) uses the ID of a
cell or point, uniquely assigned in a given data set.

Procedure using the GUI
-----------------------

For this operation, start by activating the advanced Themys mode
(``Advanced Mode``) by clicking on the corresponding button shown below.

|image0|

1. Generate global IDs
~~~~~~~~~~~~~~~~~~~~~~

Before proceeding with the selection, global IDs need to be defined if
not already present. To achieve this, start by selecting the data set
from the ``Pipeline Browser`` panel by clicking on it. Then go into
``Filters > Search...`` in the menu.

|image1|

This opens a popup which you can type into. Start typing the filter name
``Generate Global Ids`` and press ``Enter`` when it is highlighted.

|image2|

Click on ``Apply`` in the ``Properties`` panel to execute the filter.
This creates new data arrays containing unique cell IDs and point IDs,
named ``GlobalCellIds`` and ``GlobalPointIds``, respectively.

|image3|

2. Create a selection
~~~~~~~~~~~~~~~~~~~~~

Next, open the selection creation panel, called ``Find Data``, by
clicking on its icon. By default, the ``V`` key is also defined as a
keyboard shortcut to open this panel.

|image4|

In this new panel, select the data set from which to create a selection
in the ``Selection Criteria`` section with the combo box
``Data Producer``. Then choose whether to select cells or points using
the combo box ``Element Type``. To create selections, any data array can
be used. However, in this context, we will use the global IDs arrays,
which can be selected as illustrated below.

|image5|

The line below ``Element Type`` is a selection condition which can be
chosen among the following types :

-  ``<array> is <value>`` : Selects elements for which
   ``<array> = <value>``.

-  ``<array> is in range <min> and <max>`` : Selects elements for which
   ``<min> < <array> < <max>``.

-  ``<array> is one of <value1>, <value2>, <value3>, ...`` : Selects
   elements for which the value of ``<array>`` is equal to one of the
   values in the list (separated by commas).

-  ``<array> is >= value`` : Selects elements for which
   ``<array> >= <value>``.

-  ``<array> is <= value`` : Selects elements for which
   ``<array> <= <value>``.

-  ``<array> is min`` : Selects the element(s) with the smallest
   ``<array>`` value.

-  ``<array> is max`` : Selects the element(s) with the largest
   ``<array>`` value.

-  ``<array> is NaN`` : Selects elements for which the value of
   ``<array>`` is ``NaN`` (Not a Number).

-  ``<array> is <= mean`` : Selects elements for which the value of
   ``<array>`` is smaller or equal to the mean value of ``<array>``.

-  ``<array> is >= mean`` : Selects elements for which the value of
   ``<array>`` is larger or equal to the mean value of ``<array>``.

-  ``<array> is mean <value>`` : Selects elements for which
   ``(m - <value>) <= <array> <= (m + <value>)``, where ``m`` is the
   mean value of ``<array>``.

Clicking on the ``+`` button on the right of the condition adds a new
one that is combined with the others using a logical ``and``. In other
words, an element should satisfy all conditions to be selected.

Once all conditions are defined, click on the ``Find Data`` button to
create the selection. Selected elements appear in a list in the
``Selected Data`` section, as well as highlighted in the visualization
window.

|image6|

To delete the current selection, click on the ``Clear Selection`` button
on top of the visualization window illustrated below.

|image7|

3. Expand a selection
~~~~~~~~~~~~~~~~~~~~~

If you wish to expand a selection by using a different set of selection
criteria, click on the ``Freeze`` button shown below to save the current
selection. The selection criteria are then reset, allowing to define new
conditions.

|image8|

4. Extract a selection
~~~~~~~~~~~~~~~~~~~~~~

To extract the current selection and obtain a subset of the original
data set, click on the ``Extract`` button shown below. This adds an
extraction filter in the ``Pipeline Browser`` panel on the left. Click
on ``Apply`` in the ``Properties`` panel to obtain the extracted
selection.

|image9|

Procedure using Python scripting
--------------------------------

.. _generate-global-ids-1_en:

1. Generate global IDs
~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'blow.vtk' according to the desired data set
   mySource = FindSource('blow.vtk')

   # Create a new 'GenerateGlobalIds' filter
   generator = GenerateGlobalIds(Input=mySource)

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   generatorDisplay = Show(generator, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   generatorDisplay.SetScalarBarVisibility(renderView, True)

.. _create-a-selection-1_en:

2. Create a selection
~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Create a query selection
   # Choose the query type according to your needs
   # Combine several queries using '&'

   # <array> is <value>
   QuerySelect('(GlobalCellIds == 10)', 'CELL')

   # <array> is in range <min> and <max>
   QuerySelect('(GlobalCellIds > 15) & (GlobalCellIds < 20)', 'CELL')

   # <array> is one of <value1>, <value2>, <value3>, ...
   QuerySelect('(in1d(GlobalPointIds, [10, 20, 30]))', 'POINT')

   # <array> is >= value
   QuerySelect('(GlobalCellIds >= 241)', 'CELL')

   # <array> is <= value
   QuerySelect('(GlobalCellIds <= 244)', 'CELL')

   # <array> is min
   QuerySelect('(GlobalPointIds == min(GlobalPointIds))', 'POINT')

   # <array> is max
   QuerySelect('(GlobalPointIds == max(GlobalPointIds))', 'POINT')

   # <array> is NaN
   QuerySelect('(isnan(GlobalCellIds))', 'CELL')

   # <array> is <= mean
   QuerySelect('(GlobalPointIds <= mean(GlobalPointIds))', 'POINT')

   # <array> is >= mean
   QuerySelect('(GlobalPointIds >= mean(GlobalPointIds))', 'POINT')

   # <array> is mean <value>
   QuerySelect('(abs(GlobalCellIds - mean(GlobalCellIds)) <= 10)', 'CELL')

   # Clear selection if needed
   ClearSelection()

.. _expand-a-selection-1_en:

3. Expand a selection
~~~~~~~~~~~~~~~~~~~~~

Expanding through a freeze is not available in ``paraview.simple``.

.. _extract-a-selection-1_en:

4. Extract a selection
~~~~~~~~~~~~~~~~~~~~~~

.. code:: py

   # Create a new 'ExtractSelection' filter
   extractor = ExtractSelection(Input=generator)

   # Show output data
   extractorDisplay = Show(extractor, renderView)

   # Hide original source data
   Hide(generator, renderView)

   # Show color bar
   extractorDisplay.SetScalarBarVisibility(renderView, True)

.. |image0| image:: ../../img/procedures/07AdvancedMode.png
.. |image1| image:: ../../img/procedures/07FiltersMenu.png
.. |image2| image:: ../../img/procedures/07FiltersSearch.png
.. |image3| image:: ../../img/procedures/07GenerateGlobalIdsArray.png
.. |image4| image:: ../../img/procedures/07FindData.png
.. |image5| image:: ../../img/procedures/07FindDataInitial.png
.. |image6| image:: ../../img/procedures/07FindDataApply.png
.. |image7| image:: ../../img/procedures/07FindDataClear.png
.. |image8| image:: ../../img/procedures/07FindDataFreeze.png
.. |image9| image:: ../../img/procedures/07FindDataExtract.png
