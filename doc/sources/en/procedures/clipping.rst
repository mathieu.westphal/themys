Documentation: `doc fr <../../fr/procedures/clipping.html>`__



Clip a data set
===============

Description
-----------

The clipping operation removes the part of a data set located on one
side of a given clipping geometry.

Procedure using the GUI
-----------------------

Start by selecting the data set to clip from the ``Pipeline Browser``
panel by clicking on it. Then click on the ``Clip`` filter in the filter
toolbar as shown below.

|image0|

The default clipping geometry is the plane, which appears in red in the
visualization window. You can interact directly with this plane in the
following ways:

-  by clicking on the plane itself to move it along its axis

-  by clicking on the axis to adjust its orientation

-  by clicking on the ball at the center of the axis to move the plane
   origin

|image1|

To set these parameters with more precision, the coordinates for the
position and normal of the clipping plane can also be specified in the
``Properties`` panel. Afterwards, click on ``Apply`` to execute the
filter.

To allow for easier interaction with the data set, untick the
``Show Plane`` option to hide the clipping plane as shown below.

|image2|

To invert the result of the clipping operation and obtain the part of
the data set on the other side of the clipping plane, tick/untick the
``Invert`` checkbox.

|image3|

If needed, clip filters can be chained to clip a data set using more
than one plane.

|image4|

Other geometries besides the plane are available, including box,
cylinder, and sphere. An additional ``Scalar`` mode clips data sets
based on a selected scalar data array, similar to a thresholding
operation but with cell clipping.

|image5|

|image6|

|image7|

|image8|

Procedure using Python scripting
--------------------------------

1. Plane
~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'can.ex2' according to the data set to clip
   mySource = FindSource('can.ex2')

   # Create a new 'Clip' filter
   clip = Clip(Input=mySource)

   # If needed, change the origin and normal of the clipping plane
   clip.ClipType.Origin = [0.0, 0.0, 0.0]
   clip.ClipType.Normal = [0.0, 1.0, 0.0]

   # Optionally invert the output data
   clip.Invert = 0

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   clipDisplay = Show(clip, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   clipDisplay.SetScalarBarVisibility(renderView, True)

2. Box
~~~~~~

.. code:: py

   # Change clipping geometry to a box
   # Note that rotation is expressed in degrees
   clip.ClipType = 'Box'
   clip.ClipType.Position = [0.0, 0.0, 1.0]
   clip.ClipType.Rotation = [0.0, 45.0, 0.0]
   clip.ClipType.Length = [5.0, 5.0, 10.0]

3. Cylinder
~~~~~~~~~~~

.. code:: py

   # Change clipping geometry to a cylinder
   clip.ClipType = 'Cylinder'
   clip.ClipType.Center = [0.0, 0.0, -3.0]
   clip.ClipType.Axis = [0.5, 1.0, 0.5]
   clip.ClipType.Radius = 3.0

4. Sphere
~~~~~~~~~

.. code:: py

   # Change clipping geometry to a sphere
   clip.ClipType = 'Sphere'
   clip.ClipType.Center = [1.0, 0.0, 0.0]
   clip.ClipType.Radius = 5.0

5. Scalar
~~~~~~~~~

.. code:: py

   # Change clip type to scalar mode
   # Use 'CELLS' instead of 'POINTS' to use a cell array
   # Replace 'ids' with a valid scalar array
   clip.ClipType = 'Scalar'
   clip.Scalars = ['POINTS', 'ids']
   clip.Value = 1234

.. |image0| image:: ../../img/procedures/02ClipFilterSelection.png
.. |image1| image:: ../../img/procedures/02ClipFilterAppearance.png
.. |image2| image:: ../../img/procedures/02ClipFilterHidePlane.png
.. |image3| image:: ../../img/procedures/02ClipFilterInvert.png
.. |image4| image:: ../../img/procedures/02ClipFilterMultiple.png
.. |image5| image:: ../../img/procedures/02ClipBox.png
.. |image6| image:: ../../img/procedures/02ClipCylinder.png
.. |image7| image:: ../../img/procedures/02ClipSphere.png
.. |image8| image:: ../../img/procedures/02ClipScalar.png
