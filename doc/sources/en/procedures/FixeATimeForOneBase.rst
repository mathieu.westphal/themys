Documentation: `doc fr <../../fr/procedures/FixeATimeForOneBase.html>`__



Fixe a time for one base
========================

Description
-----------

Themys/ParaView allows to open several simulation times.

By default, when a new database is opened, it contributes to the list of
times offered in ParaView.

Selecting another simulation time reminds the readers to load the
simulation time closest to this value (from below).

Fixe a time for an base
-----------------------

Today, the procedure is as follows:

Step 1
------

We select the desired simulation time in the GUI.

Step2
-----

We click with the right button on the reader of the database concerned
in the pipeline to bring up a contextual drop-down menu which then
offers a checkbox to ignore (subsequently) time changes: “Ignore Time”.

Any new choice of a simulation time in the GUI will have no effect on
the reader of this database, as long as this button is checked.

To be able to change the base simulation time, simply uncheck this
button.

Note that the value of the simulation time fixed for this base is no
longer accessible.

In the Readers of Hercules, the time corresponding to the last loading
is visible. However, updating the property value may require clicking
“Refresh Information”.
