Documentation: `doc fr <../../fr/procedures/rescale_range_mode.html>`__



Rescale Range Mode
==================

Description
-----------

At the level of the **Color Map Editor View**, it is possible to choose different **Automatic Rescale Range Mode**
that we will go through them here.

|image0|

Automatic Rescale Range Mode vs Manual Rescale Range Mode
---------------------------------------------------------

**Manual Rescale Range Mode** and **Automatic Rescale Range Mode**, are available through the following buttons from top to bottom :

|image1|

# **Rescale to data range**: minimum and maximum values are the extremum values of the data loaded at the current timestep.

# **Rescale to custom range**: minimum and maximum values ​​are defined by the user;

# **Rescale to data range over all timesteps**: minimum and maximum values are the extremum values of the data among all timesteps. As a traversal of all simulation times to determine all possible values is needed, this can be expensive depending on which elements you have loaded;

# **Rescale to visible range**: minimum and maximum values are the extremum values of the data displayed at the current timestep.

.. warning::
   The **Rescale to visible range** seems to pose some problems. To be validated on 0.0.13.

**Rescale to data range over all timesteps** and **Rescale to custom range** ask you to choose a rescale mode:

# Rescale and disable automatic rescaling: the **Automatic rescale range mode** is automatically set to ***Never***. This is the button pressed by default if you finish your entries by ENTER ;

# Rescale without changing the automatic mode.

Automatic Rescale Range Mode : Never
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Disable **Auto-Rescale Range Mode**.

Automatic Rescale Range Mode : Grow and update on 'Apply'
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This mode triggered a rescale only after an Apply and the values of the colormap can only be increased if greater values ​​appear.
The min value will never change and the max value will be increased each time a greater value will be found during a rescale.

Apply to all times, this will result to cover from the min of the min to the max of the max for all values encountered.
This corresponds to applying a **Rescale to data range over all timesteps**.

Automatic Rescale Range Mode : Grow and update every timestep
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Identical to **Grow and update on 'Apply'**, however rescale is triggered at each timestep.

If apply to all times, a complete course will rescale the range mode to cover from the min of the min of the values ​​encountered to the max of the max. This corresponds to applying a **Rescale to data range over all timesteps**.

Automatic Rescale Range Mode : Update on 'Apply'
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This mode triggered a rescale only after an Apply and set the range to the values ​​proposed for the current simulation time as closely as possible. The minimum and the maximum of the colormap will change at each rescale and could be greater or smaller the previous values.

Automatic Rescale Range Mode : Clamp and update every timestep
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Identical to **Update on 'Apply'**, however rescale is triggered at each timestep.

Procedure using Python scripting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Set an Automatic Rescale Range Mode

..code:: py
    # get color transfer function/color map for 'EQPS'
    eQPSLUT = GetColorTransferFunction('EQPS')

    # Properties modified on eQPSLUT
    eQPSLUT.AutomaticRescaleRangeMode = 'Grow and update every timestep'

2. Set a Rescale to custom range

..code:: py
    # get color transfer function/color map for 'EQPS'
    eQPSLUT = GetColorTransferFunction('EQPS')

    # Rescale transfer function
    eQPSLUT.RescaleTransferFunction(1.0, 2.0)

Set range [1., 2.] and set the **Automatic rescale range mode** to ***Never***.

.. |image0| image:: ../../img/procedures/ColorMapEditor.png
.. |image1| image:: ../../img/procedures/ColorMapEditor_ManualRescale.png
