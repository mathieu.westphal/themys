add_subdirectory(animation)
add_subdirectory(scripting)
add_subdirectory(themys)

set(DOC_FILES_EN
    ${DOC_FILES_EN} ${CMAKE_CURRENT_LIST_DIR}/tutos.rst
    PARENT_SCOPE)
