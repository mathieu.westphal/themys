Documentation: `doc fr <../../fr/filters/HyperTreeGridFragmentation.html>`__

HyperTreeGridFragmentation filter
=================================

Description
-----------

This fragmentation filter identifies and characterizes the fragments in a Hyper Tree Grid mesh.

A fragment is described by a group of active (unmasked) neighboring leaf cells.

In fact, the fragments are separated by masked or undefined leaf cells.

Options
-------

The following options allow to retrieve some cell fields according to a specific semantic:

* ``EnableMassName`` and ``MassName`` declare a cell field describing a mass;
* ``EnableDensityName`` and ``DensityName`` declare a cell field describing a density/volumic mass;
* ``EnableVelocityName`` and ``VelocityName`` declare a cell field describing a velocity;

Other options:

* ``ExtractEdge`` extract centers of cells that define a fragment and that are located at the edge of the fragment.
  A cell located at the edge has at least a neighboring cell which is masked or missing. Thus, in parallel execution
  (use of more than one server), the use of this option requires former application of the ``GhostCellGenerator`` filter.

Output
------

Depending of the selected options, some global fields attached to the fragments will be
present in the output.

Each of those fields, will be exposed as a ``FieldData``, when the fragment is described by
a ``Polydata``.

The following list, describes the naming and meaning of those fields:

  * ``FragmentId`` is the index of the fragment;
  * ``Pounds`` is the number of cells that belong to the fragment;
  * ``Volume`` is the volume of the fragment; it does not take into account the mixed characteristic of the cells (through interfaces nor volumic fraction);
  * ``Mass`` (only if ``MassName`` or ``DensityName`` has been entered) is the mass of the fragment;
  * ``Density`` (only if ``MassName`` or ``DensityName`` has been entered) is the density, i.e the ratio between the mass and the volume;
  * ``AvgVelovity`` (only if ``VelocityName`` has been entered) is the mean velocity of the fragment;
  * ``Velocity`` (only if ``VelocityName`` has been entered) is the mean velocity weighted by the mass: i.e the sum of the product of the velocity
    by the per cell mass divided by total mass of the fragment.

Version 0.2
-----------

Developed in 2022. Version 0.2 solves the problems of version 0.1 (at least Issues #1 to #4 in Fragmentation project)
by putting aside the distributed aspect.

Input
~~~~~

With version 0.2, the input has changed a bit.

Prevent the output of the description of the outline of each of the fragments is still present as an option.

Associating a quantity carried by the mesh cells to a semantic is a new possibility.

This quantities are:

    * ``Mass``: the mass in each cell;
    * ``Density``: the density in each cell;
    * ``Velocity``: the velocity in each cell.

A checkbox allows to take into account the chosen field of values.

Enter the name of a value field that represents mass or density to enable the output of these fields.

Similarly, the average speed is part of the output if the name of a speed value field is given.

Associating the name of a field, which represents a mass or a density, with the one that represents a speed,
produces mass weighted speed field in the outputs.

Output
~~~~~~

The output remains unchanged between version 0.1 and 0.2 except the list of value fields and the way to calculate them (wrong in version 0.1).

Value fields that are still present:

    * for ``Centers`` under ``CellData``;
    * for ``Fragment_#`` under ``FieldData``. Unfortunately, currently `ParaView` does not allow to colorize a mesh according to a FieldData.

The value fields attached to a fragment are as follows :

    * ``FragmentId``: continuous numbering of fragments starting with zero; the naming of the block describing this fragment conforms to this value (unlike version 0.1.0) ;
    * ``Pounds``: the number of cells that contributes to this fragment;
    * ``Volume``: the total volume;
    * ``Mass``: the total mass;
    * ``Density``: the density (i.e the ``Mass`` divided by ``Volume``) ;
    * ``AvgVelocity``: the average velocity, the sum of the velocities of each contributing cell divided by ``Pounds``;
    * ``Velocity``: the velocity as an extensive quantity by the mass, the quotient of the sum of the product of the
      speed by the mass of each contributing cell divided by ``Mass``.

Fact
~~~~

A manual test procedure (currently, this base cannot be available in Gitlab's CI) validates this version of the filter.

The sequence to perform is as follows:

* load the Armen case with the Hercule reader:

    * activate the ``HIc API usage`` option; allowing the loading of an AMR mesh under vtkHyperTreeGrid;
    * select only the ``Cu1`` material;
    * select the ``EqPlastique``, ``Vitesse`` and ``rho`` value fields;

* apply a threshold filter on ``EqPlastique`` for the values [0;1.1];

|image0|

* apply this Fragmentation filter with ``rho`` and ``Vitesse`` (already checkbox active) with the edges.

|image1|

You can dynamically explore in a ``RenderView`` by selecting the ``Hover Points On`` option (a red point surmounted
by a question mark) then placing the cursor on a fragment center or one of the nodes participating in the turn of a
fragment. The information relating to this fragment will then be visible in a tooltip.

It is possible to isolate the display of a fragment. Be careful, in the ``SpreadSheetView``, to select ``Attribute``
named ``Point Data`` when you explore the ``Block Name`` named ``Centers`` and to switch to ``Attribute`` named
``FieldData`` for the blocks describing the perimeter of a fragment.

|image2|

Like displaying the average velocity field from the centers of the fragments.

|image3|


Version 0.1
-----------

Developed in 2020. The prototype 0.1 did not give satisfaction.

Input
~~~~~

Output of the description of the contour of each of the fragments may be deactivated through an option.

Indisputably, it was necessary to give the name of two quantities: one which represents the density and the other a speed.

Output
~~~~~~

This filter returns a vtkMultiBlockDataSet:

* the first block ``#0`` named ``Centers`` is a ``vtkPolyData`` of points, each point characterizes the center of
  a fragment (the average of the coordinates of the cells which define a fragment) to which the fields
  of values describing this fragment as ``CellData`` is associated;
* the other blocks named ``Fragment_#``, with between ``#0`` to ``#N-1`` (N the number of fragments),
  are ``vtkPolyData``. Each of these blocks describes the edge of a fragment without privileged order
  and is associated to fields of values describing this fragment as ``FieldData``.

Fact
~~~~

It locates the centers of the fragments but with incorrect edges (sometimes cells internal to a fragment
were retained). In addition, the associated value fields (CellData or FieldData) were not correctly calculated.
The parallel version never worked. And finally, various functionalities were missing, whether at the level of
the returned value fields or the characterization of the shape of each fragment.

.. |image0| image:: ../../img/filters/HyperTreeGridFragmentation_1.png
.. |image1| image:: ../../img/filters/HyperTreeGridFragmentation_2.png
.. |image2| image:: ../../img/filters/HyperTreeGridFragmentation_3.png
.. |image3| image:: ../../img/filters/HyperTreeGridFragmentation_4.png
