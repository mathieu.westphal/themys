Documentation: `doc fr <../../fr/readers/HerculesReader_chap2.html>`__



La grande révolution par rapport à LOVE
---------------------------------------

L'utilisation standard de **LOVE** faisait en sorte que lorsqu’on cliquait sur un champ de valeurs
(dénommé parfois *grandeur* ou *propriété*) cela supprimait de la mémoire le champ de valeur actif
pour charger en mémoire le champ de valeur nouvellement sélectionné.
Le rendu était ensuite réalisé avec ce champ de valeur.

Dans **Themys**, l'utilisation est dissociée en deux étapes :

* une de chargement de/s champ/s de valeurs pilotable à travers les **Properties** du lecteur de base Hercule, et
* une d’utilisation d'un champ de valeurs chargé en mémoire à travers le reste de la GUI de **Themys**.

Dans la phase de chargement, l'utilisateur peut sélectionner les **maillage**, **matériau** et
champs de valeurs aux cellules et aux noeuds qu'il compte utiliser dans son expérience interactive.
Par la suite, l'utilisateur pourra à tout moment modifier ces sélections en ajoutant ou supprimant
un élément à charger, modification à valider en appuaynt sur le bouton **Apply** (appliquer).

.. warning::
   Afin d'optimiser votre expérience interactive, nous vous conseillons de limiter le
   chargement au strict nécessaire en sélectionnant les **maillage**, **matériau** et champs de
   valeurs aux cellules et aux noeuds qui sont digne d'intérêt. Le choix de ces sélections est pris
   en compte lorsqu'on change de temps ou lorsqu'on fait une requête sur tous les temps de simulation.
