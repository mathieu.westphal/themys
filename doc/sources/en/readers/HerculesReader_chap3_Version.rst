Documentation: `doc fr <../../fr/readers/HerculesReader_chap3_Version.html>`__



Version
^^^^^^^

La propriété **Version** n'est disponible qu'en mode avancée, elle permet d'afficher la version du lecteur Hercule
en indiquant le numéro de version comme la date d'installation.
