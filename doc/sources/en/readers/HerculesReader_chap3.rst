Documentation: `doc fr <../../fr/readers/HerculesReader_chap3.html>`__



Propriétés
----------

.. toctree::
   :maxdepth: 1

   HerculesReader_chap3_Version
   HerculesReader_chap3_CurrentTime
   HerculesReader_chap3_FixedTime
   HerculesReader_chap3_TimeShift
   HerculesReader_chap3_RefreshInformation
   HerculesReader_chap3_ReloadImmediately
   HerculesReader_chap3_HIcAPIUsage
   HerculesReader_chap3_MeshArray
   HerculesReader_chap3_MaterialArray
   HerculesReader_chap3_DataArray
   HerculesReader_chap3_SpectralRangeIndices
   HerculesReader_chap3_MemoryEfficient
   HerculesReader_chap3_Interface
   HerculesReader_chap3_Extruder1D
   HerculesReader_chap3_TB-AMR

|image0|

Un exemple d'affichage des propriétés du filtre de lecteur Hercule.

.. |image0| image:: ../../img/readers/hercules_reader.png
