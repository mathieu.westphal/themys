Documentation: `doc fr <../../fr/readers/HerculesReader_chap3_Interface.html>`__



Code Interface
^^^^^^^^^^^^^^

Une information subpixel est parfois décrite dans les bases Hercule.
Elle consiste à décrire un ou deux plans parallèles d'interface par matériau afin d'affiner la présence
spatiale du matériau dans une cellule.

Cas A, si la propriété `HIc API usage <HerculesReader_chap3_HIcAPIUsage.html>`_ n'est pas activée et
que ce n'est pas un maillage TB-AMR, alors les informations relatives à la description de ces
interfaces, si elles existent, sont systématiquement chargées en mémoire.
Néanmoins, pour bénéficier du rendu, il faudra encore appliquer un filtre de traitement
**vtkMaterialInterface** qui par défaut sélectionnera au niveau des propriétés les champs
nécessaires à l'application de ce filtre.

Cas B, si la propriété `HIc API usage <HerculesReader_chap3_HIcAPIUsage.html>`_ est activée et
que c'est un maillage TB-AMR, alors rien n'est fait par défaut. Néanmoins, l'utilisateur
peut alors sélectionner un des deux champs de valeurs préfixés par *vtkInterface*.
Ce chargement en mémoire sera par la suite automatiquement exploité lors du rendu afin
de subdiviser les cellules.

.. note::
   Le chargement de cette description ayant un coût relativement important puisqu'il est équivalent
   à deux champs vectoriels, il est envisagé de le rendre optionnel d'où l'existence de cette propriété.
   Ce qui donnerait le comportement suivant :

   * Dans le cas A, la décision de charger la description de l'interface sera fonction de l'état
     de cette propriété.
     L'activation de cette propriété pourrait ensuite activer automatiquement l'application du filtre qui
     découpe les cellules en fonction de ces interfaces.

   * Dans le cas B, le choix du chargement ne se ferait plus à travers les champs de valeurs
     **Cell Data Array** mais au niveau de cette propriété.
