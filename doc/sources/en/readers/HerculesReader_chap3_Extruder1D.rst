Documentation: `doc fr <../../fr/readers/HerculesReader_chap3_Extruder1D.html>`__



Extruder un maillage 1D
^^^^^^^^^^^^^^^^^^^^^^^

Le chargement d’un maillage **vtkStructuredGrid** 1D est automatiquement transformé en un **vtkStructuredGrid** 2D
sous la forme d’une *queue de paon*.

Les propriétés de cette transformation sont décrites par :

* ``1D sector nb``, qui fixe le nombre de cellules,
* ``1D sector min``, qui détermine la position du premier secteur, et
* ``1D sector max``, qui détermine la position du dernier secteur.

.. note::
   N'oubliez pas ensuite de cliquer sur **Apply** (appliquer) dans **Properties** de l'instance du lecteur modifiée.
