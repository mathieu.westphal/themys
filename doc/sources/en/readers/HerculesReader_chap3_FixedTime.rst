Documentation: `doc fr <../../fr/readers/HerculesReader_chap3_FixedTime.html>`__



Fixed Time
^^^^^^^^^^

Le mécanisme générale de sélection d'un temps de simulation au niveau de la **GUI Themys** et sa traduction
par un ordre de chargement au niveau d'une instance du lecteur Hercule est décrite avec la propritété
`Current Time <HerculesReader_chap3_CurrentTime.html>`_.

Cette propriété propose de maîtriser le choix du temps de simulation à charger pour une instance préise
du lecteur Hercule.

Le choix se fait à travers le positionnement dans un menu déroulant :

* commençant par une *zone vide*, puis
* comprenant la liste des temps de simulation qui sont disponible dans cette base.

Le positionnement sur la *zone vide* permet de désactiver cette propriété, la **GUI Themys** retrouve son rôle
initial.

Le positionnement sur un temps de simulation va, quand à lui, fixer, punaiser de façon immuable le temps de
chargement pour cette instance de base. Les événements liès à un choix au niveau de la **GUI Themys** n'auront
alors plus aucun effet. La propriété `Time Shift <HerculesReader_chap3_TimeShift.html>`_ sera ignorée.

.. note::
   N'oubliez pas ensuite de cliquer sur **Apply** (appliquer) dans **Properties** de l'instance du lecteur modifiée.
