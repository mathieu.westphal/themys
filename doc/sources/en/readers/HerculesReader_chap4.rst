Documentation: `doc fr <../../fr/readers/HerculesReader_chap4.html>`__



Configuration utilisateur par défaut
------------------------------------

Les utilisateurs ont souhaité pouvoir prédéfinir des listes par défaut d'objets à sélectionner.

Cette configuration est décrite dans le fichier utilisateur ~/.config/CEA/themys-UserSettings.json
et de la façon suivante :

| {
|   "HerculeReader":
|   {
|     "DefaultSelectedMeshes" : ["meshNS", "meshNSYZ"],
|     "DefaultSelectedMaterials" : ["MatB", "global_meshNSYZ"],
|     "DefaultSelectedNodeFields" : ["Noeud:node_densite"],
|     "DefaultSelectedCellFields" : ["Milieu:mil_densite", "Maillage:cell_vecteurA1"]
|   }
| }

La description de cette configuration est suffisamment lisible pour ne pas nécessiter plus de détail.

Pour chacune des listes, en l'absence d'un défaut de liste de sélection ou d'objet sélectionné lors de
l'application de cette liste par défaut lors de l'ouverture d'une base, ce sera le défaut codé dans le
lecteur qui sera pris, c'est-à-dire :

* seul le premier maillage par ordre alphabétique est sélectionné,
* tous les matériaux sont sélectionnés sauf ceux préfixés par *global_* ;
* **vtkPointId** est sélectionné pour les champs de valeurs aux noeuds ;
* **vtkCellId** et **vtkDomainId** sont sélectionnés pour les champs de valeurs aux cellules.
