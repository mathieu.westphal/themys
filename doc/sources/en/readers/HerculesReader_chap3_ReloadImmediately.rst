Documentation: `doc fr <../../fr/readers/HerculesReader_chap3_ReloadImmediately.html>`__



Reload immediately
^^^^^^^^^^^^^^^^^^

Cette propriété déclenche immédiatement le rechargement de la base.

L'action inhérente à cette propriété est plus impactante que celle de
`Refresh information (rafraîchir le contenu des propriétés) <HerculesReader_chap3_RefreshInformation.html>`_
qui ne se limitait qu'à mettre à jour le contenu des informations du temps de simulation en cours.

Cette propriété va déclencher la fermeture de la base puis sa réouverture, son positionnement au temps
demandé tout en conservant les mêmes valeurs de **Properties**.

C'est ce qui va permettre aussi de réactualiser la liste des temps de simulation disponible pour cette base,
utile lorsqu'on analyse une base en cours de calcul.

.. note::
   Contrairement aux autres propriétés du lecteur Hercule, le clic sur le bouton associé à cette propriété est
   immédiatement appliquée sans modifier l'état du bouton **Apply**.
