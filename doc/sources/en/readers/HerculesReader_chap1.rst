Documentation: `doc fr <../../fr/readers/HerculesReader_chap1.html>`__



Description
-----------

Le lecteur Hercule **CEAReaders** permet de lire des données stockées au format **Hercule**,
format de stockage des données propriétaire au CEA/DAM.

La structuration des données en **output** (sortie) est la suivante :

* Un container, **vtkMultiBlockDataSet**, décrit une liste de **bloc**/**maillage** représentant un aspect
  différent de la même simulation.

* Chaque **bloc**/**maillage** est lui-même un container, **vtkMultiBlockDataSet**, qui décrit une liste de
  **bloc**/**matériau**/**milieu**/**groupe** représentant une partie du maillage de simulation. Suivant le type de
  simulation, un chevauchement géométrique entre ces parties de maillage peut exister. Il peut être partiel ou total.
  Les cellules qui appartiennent à plusieurs parties sont dites **cellules mixtes**/**mailles mixtes**.

* Chaque **bloc**/**matériau**/**milieu**/**groupe** est lui-même un container mais d'une autre spécificité,
  **vtkMultiPieceDataSet**, qui décrit une liste de **pièce** relative à la distribution de la simulation :

  * soit celle sur les serveurs de **Themys** en mode de lecture avec l'API HS, Hercule Service ;

  * soit celle relative à la distribution de la simulation sur les processus de calcul et de la distribution de ces
    éléments sur les serveurs de **Themys** en mode de lecture avec l'API HIc, l'API des codes.

* Toutes les **pièces** d’un **vtkMultiPieceDataSet** sont nécessairement de même type de représentation :

  * structuré : **vtkImageData** (2-3D), **vtkRectilinearGrid** (2-3D), **vtkStructuredGrid** (1-2-3D),

  * non structuré : **vtkPolyData** (1-2-3D), **vtkUnstructuredGrid** (1-2-3D),

  * à raffinement adaptatif basé sur une grille d'arbre de décomposition (TB-AMR) : **vtkHyperTreeGrid** (2-3D).

Par défaut, pour un **vtkStructuredGrid** 1D, le lecteur Hercule va produire un maillage **vtkStructuredGrid** 2D
de type *queue de paon*.
Le paramètrage se fait à travers l'emploi de **Properties** spécifiques du lecteur.

Les champs de valeurs sont associès aux cellules (**CellData**, sémantiquement au centre) ou aux noeuds
(**PointData**, sauf pour la représentation **vtkHyperTreeGrid** qui n’a de sémantique aux noeuds au niveau de
la représentation) de chacune des **piece**.

Les champs de valeurs globaux (**FieldData**) sont aussi associès à chacune des **pièce**.
Ils sont globaux :

* à la **pièce**, comme :

  * **vtkFieldMaterialId** : le numéro du **matériau**/**milieu**/**groupe** ; ou

  * **vtkProcessId** : le numéro du serveur **Themys** en mode d'exécution de **Themys** client/serveurs ;

* à la simulation, le même champ de valeur global est alors dupliqué sur chacune des **pièce**, comme :

  * **vtkFixedTimeStep** : le numéro du temps actuellement chargé,

  * **vtkFixedTimeValue** : la valeur du temps actuellement chargé,

  * **Place** et **DistanceToOrigin** : le plan de symétrie avec le type du plan (0 suivant X, resp. 1 Y et 2 Z) et
    la distance à l'origine suivant l'axe concerné ;
