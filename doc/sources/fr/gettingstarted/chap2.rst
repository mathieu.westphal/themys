Documentation: `doc en <../../en/gettingstarted/chap2.html>`__



Création d’une source
=====================

Le processus de visualisation dans **Themys** commence par l’importation de vos données dans l’application.
Outre la lecture de fichiers pour importer des données dans l’application, **Themys** fournit également une
collection de sources de données pouvant produire des exemples de jeux de données. Ceux-ci sont disponibles
dans le menu **Sources**. Pour créer une source, cliquez simplement sur n’importe quel élément du menu **Sources**.

.. note::

   Lorsque vous déplacez votre curseur sur les éléments de n’importe quel menu, sur la plupart des plates-formes,
   vous verrez une brève description de l’élément dans la barre d’état dans le coin inférieur gauche de la fenêtre
   de l’application.

Si vous cliquez sur **Sources**>**Sphere**, par exemple, vous allez créer un algorithme de producteur qui génère
une surface sphérique, comme le montre la Fig. 1.2.

|image0|

Fig. 1.2 Visualisation dans Themys : Etape 1

Quelques points à noter :

* Un module de pipeline est ajouté dans le panneau **Pipeline Browser** (explorateur du pipeline)
  avec un nom dérivé de l’élément de menu, comme indiqué en surbrillance.

* Le panneau **Properties** se remplit de texte pour indiquer qu’il affiche les propriétés de l’élément
  en surbrillance (qui, dans ce cas, est *Sphere1*), ainsi que pour afficher certains widgets pour des
  paramètres tels que *Center*, *Radius*, etc.

* Dans le panneau **Properties**, le bouton **Apply** (appliquer) est activé et mis en surbrillance.

* La vue 3D n’est pas affectée, car rien de nouveau n’est encore affiché ou rendu dans cette vue.

Regardons de plus près ce qui s’est passé. Lorsque nous avons cliqué sur **Sources**>**Sphere**, nous avons
créé une instance d’une source qui peut produire un maillage de surface sphérique – c’est ce qui est reflété
dans le **Pipeline Browser**. Cette instance reçoit un nom, qui est utilisé par *Sphere1* et **Pipeline Browser**,
ainsi que d’autres composants de l’interface utilisateur, pour faire référence à cette instance de la source.
Les modules de pipeline tels que les sources et les filtres contiennent des paramètres que vous pouvez modifier
et qui affectent le comportement de ce module. Nous les appelons des propriétés. Le panneau **Properties**
affiche ces propriétés et vous permet de les modifier. Étant donné que l’ingestion de données dans le
système peut prendre beaucoup de temps, **Themys** vous permet de modifier les propriétés avant que le
module ne s’exécute ou n’effectue le traitement réel pour ingérer les données. Par conséquent, le bouton
**Apply** est mis en surbrillance pour indiquer que vous devez accepter les propriétés avant que la demande
ne soit traitée. Comme aucune donnée n’est encore entrée dans le système, il n’y a rien à montrer.
Par conséquent, la vue 3D n’est pas affectée.

Supposons que nous soyons d’accord avec les valeurs par défaut pour toutes les propriétés de *Sphere1*.
Ensuite, cliquez sur le bouton **Apply**.

|image1|

Fig. 1.3 Visualisation dans Themys : Etape 2

S’ensuivront (Fig. 1.3):

* Le bouton **Apply** revient à son ancien état désactivé/non mis en surbrillance.

* Une surface sphérique est rendue dans la vue 3D.

* La section **Display** (affichage) du panneau **Properties** affiche désormais de nouveaux paramètres ou propriétés.

* Certaines barres d’outils sont mises à jour et vous pouvez voir que les barres d’outils contenant du texte, telles que **Solid Color** (couleur unie) et **Surface**, sont maintenant activées.

En cliquant sur **Apply**, nous leur avons demandé d’appliquer les propriétés affichées dans le panneau
**Properties**. Lorsqu’une nouvelle source (ou filtre) est appliquée pour la première fois, **Themys**
affiche automatiquement les données produites par le module de pipeline dans la vue actuelle, si possible.
Dans ce cas, la source sphérique produit un maillage de surface, qui est ensuite affiché ou affiché dans la vue 3D.

Les propriétés qui vous permettent de contrôler l’affichage des données dans la vue sont désormais
affichées dans le panneau **Properties** de la section **Display** (affichage). Des éléments tels que la
couleur de surface, le type de rendu ou la représentation, les paramètres d’ombrage, etc., sont présentés
dans cette section nouvellement mise à jour.

Certaines des propriétés couramment utilisées sont également dupliquées dans la barre d’outils.
Ces propriétés incluent le tableau de données avec lequel la surface est colorée et le type de représentation.
Ce sont les modifications apportées à la barre d’outils qui vous permettent de modifier rapidement certaines
propriétés d’affichage.

.. |image0| image:: ../../img/getstarting/sphere.png
   :width: 400

.. |image1| image:: ../../img/getstarting/sphere2.png
   :width: 400
