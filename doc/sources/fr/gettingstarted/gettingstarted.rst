Documentation: `doc en <../../en/gettingstarted/gettingstarted.html>`__

Bien commencer
**************

.. toctree::
   :maxdepth: 1

   chap1
   chap2
   chap3
   chap4
