Documentation: `doc en <../../en/readers/readers.html>`__

Lecteurs
********

.. toctree::
   :maxdepth: 1

   DatReader
   HerculesReader

Section orientée pour le développeur :

.. toctree::
   :maxdepth: 1

   ReaderPipelineName
