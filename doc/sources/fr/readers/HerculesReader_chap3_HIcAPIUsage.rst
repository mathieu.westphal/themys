Documentation: `doc en <../../en/readers/HerculesReader_chap3_HIcAPIUsage.html>`__



HIc API usage
^^^^^^^^^^^^^

L’option **HIc API usage** permet de choisir le mode de chargement du lecteur Hercule :

*  *non activé*/*non sélectionné*, le mode de chargement se fait alors via l'**API HS**,
   Hercule Services. Cette API Hercule spécifique à l'analyse et la visualisation intègre différentes
   manipulations et permet d'accèder à des informations cachées de la base (non visible à travers hercule_view,
   l'application d'Hercule qui permet de visualiser le contenu brute des données écrites par le code dans une base
   Hercule qu'elle soit de dépouillement ou d'inter-code). Dans ce mode :

   *  Le chargement de maillage à raffinement adaptatif basé sur une grille d'arbre de décomposition (TB-AMR)
      se fait par conversion en **vtkPolyData** (2D) ou
      **vtkUnstructuredGrid** (3D). La topologie ne pouvant être complétement décrite correctement avec ces
      représentations, en plus d'un surcoût mémoire, les traitements locaux à une cellule nécessitant les
      informations des cellules voisines fourniront un résultat erronné. Cela concerne, par exemple,
      les filtres d'iso-contour, gradient comme le rendu de type **features edges**/silhouette.

   *  Au niveau d'un serveur, une reconstruction est faite afin d’assembler les sous-domaines de calcul de la
      simulation qui lui a été attribué. Ainsi, chaque serveur décrira qu’une **pièce**, le nombre de **pièces**
      correspondra au nombre de serveurs **Themys** en cours d'exécution.

*  *activé* : le mode de chargement se fait via **API Hercule HIc**, l’API dite des codes. Les traitements sont
   alors en charge du lecteur Hercule. Dans ce mode :

   *  Le chargement de maillage TB-AMR se fait en **vtkHyperTreeGrid**, cette représentation spécifique de VTK
      gère nominalement ce type de maillage. A cette représentation est associée des filtres spécifiques dont
      certains appliquent des traitements locaux à une cellule nécessitant les informations des cellules voisines
      comme c'est le cas du calcul d'iso-contour ou de gradient.

   *  Au niveau d'un serveur :

      * pour la représentation **vtkHyperTreeGrid**, une reconstruction est faite afin d’assembler les
        sous-domaines de calcul de la simulation qui lui a été attribué. Ainsi, chaque serveur décrira
        qu’une **pièce**, le nombre de **pièces** correspondra au nombre de serveurs **Themys** en cours d'exécution.

      * pour les autres représentaions, le nombre de **pièces** correspondra au nombre de sous-domaine de calcul
        de la simulation avec une distribution de ces **pièces** entre les serveurs **themys**.

.. note::
   Il est recommandé d'activer cette option pour le chargement de maillage à raffinement adaptatif basé sur une
   grille d'arbre de décomposition (TB-AMR) et de ne pas activer cette option pour d'autre type de maillage même
   cette option permet de les gérer dans une grande majorité des cas.

.. warning::
   Le changement d'état de cette option nécessite de cliquer sur le bouton **Apply** (appliquer).
   Une fois fait, la description du contenu de la base change au niveau des nommages des éléments.
   Une nouvelle phase des éléments à charger sera donc à réaliser.
