Documentation: `doc en <../../en/readers/HerculesReader_chap3_TB-AMR.html>`__



Propriétés HTG (TB-AMR)
^^^^^^^^^^^^^^^^^^^^^^^

Certains propriétés du lecteur permettent de cibler une partie du maillage TB-AMR à garder en mémoire.

Ces propriétés ne sont valides que si la propriété
`HIc API usage <HerculesReader_chap3_HIcAPIUsage.html>`_ est activée et que vous chargez un maillage de type
TB-AMR. La représentation d'un telle maillage est alors **vtkHyperTreeGrid** ou **HTG**.

Cela peut se faire à travers ces différentes propriétés :

* **HTG up to level max** fixe le niveau de profondeur maximal que l'on doit charger et conserver en mémoire,
  cette valeur commence à 0 pour ne conserver que le premier niveau ; en cours d'analyse, il est aussi
  possible d'appliquer un filtre **vtkHyperTreeGridDepthLimiter** qui ne réduira pas le coût mémoire (contrairement
  à cette option du lecteur) mais qui pourra accélérer l'obtention d'un résultat grossier en limitant les niveaux
  qui seront pris en compte dans l'application des filtres suivant.

* **HTG active bounding box** active ou non la prise en compte de la propriété suivante
  **HTG bounding box by coordinates**.

* **HTG bounding box by coordinates** fixe spatialement les arbres de décomposition (**vtkHyperTree**) qui
  seront retenues après le chargement.
