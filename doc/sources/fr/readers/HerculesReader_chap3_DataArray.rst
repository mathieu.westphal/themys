Documentation: `doc en <../../en/readers/HerculesReader_chap3_DataArray.html>`__



Cell and Point Data Array
^^^^^^^^^^^^^^^^^^^^^^^^^

Les propriétés **Cell Data Array** et **Point Data Array** listent les différents champs de valeurs disponibles
sur les différents matériaux présents dans les différents maillages de simulation.
L'utilisateur peut ainsi sélectionner les champs de valeurs de la simulation qui lui semblent d'intérêt
pour l'analyse à réaliser.

Les valeurs aux cellules sont données au centre des cellules.

Les champs de valeurs préfixés par :

* **Maillage** sont liés aux champs de valeurs positionnés sur un matériau préfixé par **global_** ;
* **Milieu** sont liés aux champs de valeurs positionnés sur un matériau non préfixé par **global_**.

Ainsi, le fait de sélectionner **Material:Density** a pour effet de charger tous les champs de
valeurs **Density** présents au niveau des matériaux sélectionnés.

Cela se complique quelque peut lorsqu'on introduit l'aspect **Constituant** et **Fluide** dont les noms des
champs seront forcément préfixés par le nom du constituant ou fluide qui est un des composants d'un matérial,
ceci afin de les différencier entre eux. Si chacun des matériaux a des constituants et fluides différents
cela multiplie d'autant la liste des champs de valeurs proposés.

Généralement, cette sélection est à faire après celle sur les maillages de simulation
`Mesh Array (sélection des maillages de simulation) <HerculesReader_chap3_MeshArray.html>`_ et sur les
matériaux `Material Array (sélection de matériaux) <HerculesReader_chap3_MaterialArray.html>`_.
Bien entendu, ces choix devront être cohéents. Ce n'est qu'à l'issue de toutes ces sélections positionnées
que l'utilisateur pourra confirmer cet choix et déclencher le chargement en cliquant sur **Apply**.

A tout moment, l'utilisateur peut revenir sur ses choix.

.. warning::
   Il est conseillé de sélectionner le juste nécessaire car c'est ce qui sera chargé lors d'un changement de
   temps de simulation comme lors d'un parcours temporel pour les services proposés par la **GUI Themys**
   de type *over times*.
