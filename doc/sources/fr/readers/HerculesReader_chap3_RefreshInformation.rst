Documentation: `doc en <../../en/readers/HerculesReader_chap3_RefreshInformation.html>`__



Refresh information
^^^^^^^^^^^^^^^^^^^

Cette propriété ne doit nominalement pas être utilisée.

Néanmoins son usage peut être dicté pour des bases Hercule mal formées avec des contenus variables au
cours du temps. Cela peut arriver généralement si l'option **API HIc Usage** a été sélectionnée dans les
**Properties** du lecteur.

Cette propriété s'utilise après un changement de temps de simulation au niveau de la **GUI Themys**.

Cette propriété permet de forcer le rafraichissement du contenu des différentes listes présentées par ce lecteur.

.. note::
   Contrairement aux autres propriétés du lecteur Hercule, le clic sur le bouton associé à cette propriété est
   immédiatement appliquée sans modifier l'état du bouton **Apply**.
