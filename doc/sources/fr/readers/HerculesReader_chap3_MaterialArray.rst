Documentation: `doc en <../../en/readers/HerculesReader_chap3_MaterialArray.html>`__



Material Array
^^^^^^^^^^^^^^

La propriété **Material Array** liste les différents matériaux présents dans les différents maillages de simulation.
L'utilisateur peut ainsi sélectionner les matériaux de simulation qui lui semblent d'intérêt
pour l'analyse à réaliser.

Les matériaux préfixés par **global_** correspondent à la représentation globale de chacun des maillages de
simulation de la base. En effet, certains champs de valeurs n'étant disponibles qu'au niveau global du maillage.

Par défaut, ils ne sont pas sélectionnés car fort consommateur de mémoire et inadapté à un chargement ciblé des
matériaux.

Néanmoins dans certains cas, il n'y a pas d'autre choix que de sélectionner la version globale d'un
maillage de simulation qui n'aurait pas de déclinaison par matériau. C'est généralement le cas des nuages de
particules ou des rayons.

Les noms des matériaux sont préfixés par un numéro qui correspond à la valeur attribuée au champ de valeurs
**vtkMaterialId** et **vtkFieldMaterialId**.

Généralement, cette sélection est à faire après celle sur les maillages de simulation
`Mesh Array (sélection des maillages de simulation) <HerculesReader_chap3_MeshArray.html>`_ et avant celles des champs
de valeurs `Cell and Point Data Array (sélection de champ de valeurs) <HerculesReader_chap3_DataArray.html>`_.
Bien entendu, ces choix devront être cohéents. Ce n'est qu'à l'issue de toutes ces sélections positionnées
que l'utilisateur pourra confirmer cet choix et déclencher le chargement en cliquant sur **Apply**.

A tout moment, l'utilisateur peut revenir sur ses choix.

.. warning::
   Il est conseillé de sélectionner le juste nécessaire car c'est ce qui sera chargé lors d'un changement de
   temps de simulation comme lors d'un parcours temporel pour les services proposés par la **GUI Themys**
   de type *over times*.
