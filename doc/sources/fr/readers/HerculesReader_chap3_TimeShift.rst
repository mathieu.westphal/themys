Documentation: `doc en <../../en/readers/HerculesReader_chap3_TimeShift.html>`__



Time Shift
^^^^^^^^^^

Le mécanisme générale de sélection d'un temps de simulation au niveau de la **GUI Themys** et sa traduction
par un ordre de chargement au niveau d'une instance du lecteur Hercule est décrite avec la propritété
`Current Time <HerculesReader_chap3_CurrentTime.html>`_.

Cette propriété offre la possibilité d'appliquer un décalage (valeur positive ou négative) au temps demandé
à travers la **GUI Themys**. Cela peut être pratique dans la comparaison de deux simulations n'ayant pas opté pour
la même origine temporelle.

La valeur de cette propriété est tout simplement ajoutée au temps de simulation demandé par la **GUI Themys**.
C'est ce temps qui est alors utilisé en interne au lecteur et sur lequel on applique alors le modus operanti décrit
dans `Current Time <HerculesReader_chap3_CurrentTime.html>`_ (précisément le temps demandé, ou le temps
le plus proche par en-dessous ou à défaut le temps le plus proche par au-dessus).

Le positionnement de cette propriété à 0 revient à la désactiver.

Cette propriété est ignorée si un temps a été fixé par la propriété
`Fixed Time <HerculesReader_chap3_FixedTime.html>`_.

.. note::
   N'oubliez pas ensuite de cliquer sur **Apply** (appliquer) dans **Properties** de l'instance du lecteur modifiée.
