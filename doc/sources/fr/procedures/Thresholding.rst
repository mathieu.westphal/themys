Documentation: `doc en <../../en/procedures/Thresholding.html>`__



Threshold a data set
====================

Description
-----------

The threshold operation on a data set extracts cells with data values
below or above a given threshold value.

Procedure using the GUI
-----------------------

Start by selecting the data set to threshold from the
``Pipeline Browser`` panel by clicking on it. Then click on the
``Threshold`` filter in the filter toolbar as shown below.

|image0|

In the ``Properties`` panel, choose the scalar data array to use for
thresholding with the ``Scalars`` combo box.

Then select the type of thresholding using the ``Threshold Method``
combo box:

-  ``Between`` extracts cells with values between the lower and upper
   thresholds.

-  ``Below Lower Threshold`` extracts cells with values smaller than the
   lower threshold.

-  ``Above Upper Threshold`` extracts cells with values larger than the
   upper threshold.

Next, the threshold values (``Lower Threshold`` et ``Upper Threshold``)
can be adjusted with the corresponding sliders or specified directly.

|image1|

To invert the result of the thresholding operation, tick the ``Invert``
option as shown below.

|image2|

Procedure using Python scripting
--------------------------------

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the data set to threshold
   mySource = FindSource('Wavelet1')

   # Create a new 'Threshold' filter
   threshold = Threshold(Input=mySource)

   # If needed, change scalar data array
   # Use 'CELLS' instead of 'POINTS' to use a cell array
   # Replace 'RTData' with a valid scalar array
   threshold.Scalars = ['POINTS', 'RTData']

   # If needed, change threshold type (default is 'Between')
   threshold.ThresholdMethod = 'Below Lower Threshold'
   # or
   threshold.ThresholdMethod = 'Above Upper Threshold'
   # or
   threshold.ThresholdMethod = 'Between'

   # Set threshold values
   threshold.LowerThreshold = 70.0
   threshold.UpperThreshold = 250.0

   # Optionally invert the output data
   threshold.Invert = 1

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   thresholdDisplay = Show(threshold, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   thresholdDisplay.SetScalarBarVisibility(renderView, True)

.. |image0| image:: ../../img/procedures/05ThresholdFilterSelection.png
.. |image1| image:: ../../img/procedures/05ThresholdFilterOptions.png
.. |image2| image:: ../../img/procedures/05ThresholdFilterInvert.png
