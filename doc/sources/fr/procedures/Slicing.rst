Documentation: `doc en <../../en/procedures/Slicing.html>`__



Slice a data set
================

Description
-----------

The slicing operation extracts the intersection between a given geometry
and a data set.

Procedure using the GUI
-----------------------

Start by selecting the data set to slice from the ``Pipeline Browser``
panel by clicking on it. Then click on the ``Slice`` filter in the
filter toolbar as shown below.

|image0|

The default slicing geometry is the plane, which appears in red in the
visualization window. You can interact directly with this plane in the
following ways:

-  by clicking on the plane itself to move it along its axis

-  by clicking on the axis to adjust its orientation

-  by clicking on the ball at the center of the axis to move the plane
   origin (not visible in the picture below)

|image1|

To set these parameters with more precision, the coordinates for the
position and normal of the slicing plane can also be specified in the
``Properties`` panel. Afterwards, click on ``Apply`` to execute the
filter.

To allow for easier interaction with the data set, untick the
``Show Plane`` option to hide the slicing plane as shown below.

|image2|

It is possible to produce more than one slice. To achieve this, first
enable advanced options by clicking on the ``Advanced Properties`` wheel
button illustrated below. A list containing position offsets from the
current slicing plane then appears. You can define as many slices as you
wish.

|image3|

Various options are available to modify the list of slices, detailed
below.

|image4|

1. **``+`` button** : Adds a new value to the list below the currently
   selected value.

2. **``-`` button** : Deletes the selected value from the list.

3. **Automatic values generator button** : Opens a window offering
   options to generate offset values distributed in different manners
   (linearly, logarithmically, etc.) in a given range.

   |image5|

4. **Delete button** : Deletes all values from the list.

5. **Scaling button** : Divides or multiplies all values by 2.

6. **Reset button** : Resets the list to its original state.

To modify a value, simply click on it and enter a new value.

Other geometries besides the plane are available, including box,
cylinder, and sphere.

|image6|

|image7|

|image8|

Procedure using Python scripting
--------------------------------

1. Plane
~~~~~~~~

.. code:: py

   # Find the source data set with its name
   # Replace 'Wavelet1' according to the data set to slice
   mySource = FindSource('Wavelet1')

   # Create a new 'Slice' filter
   slice = Slice(Input=mySource)

   # If needed, change the origin and normal of the slicing plane
   slice.SliceType.Origin = [3.0, 0.0, 0.0]
   slice.SliceType.Normal = [1.0, 1.0, 0.0]

   # Optionally add more slices along the defined Normal (0.0 corresponds to the slice defined with Origin)
   slice.SliceOffsetValues = [0.0, 5.0, -5.0]

   # Get active render view
   renderView = GetActiveViewOrCreate('RenderView')

   # Show output data
   sliceDisplay = Show(slice, renderView)

   # Hide original source data
   Hide(mySource, renderView)

   # Show color bar
   sliceDisplay.SetScalarBarVisibility(renderView, True)

2. Box
~~~~~~

.. code:: py

   # Change slicing geometry to a box
   # Note that rotation is expressed in degrees
   slice.SliceType = 'Box'
   slice.SliceType.Position = [0.0, 0.0, 1.0]
   slice.SliceType.Rotation = [0.0, 45.0, 0.0]
   slice.SliceType.Length = [5.0, 5.0, 10.0]

3. Cylinder
~~~~~~~~~~~

.. code:: py

   # Change slicing geometry to a cylinder
   slice.SliceType = 'Cylinder'
   slice.SliceType.Center = [0.0, 0.0, -3.0]
   slice.SliceType.Axis = [0.5, 1.0, 0.5]
   slice.SliceType.Radius = 3.0

4. Sphere
~~~~~~~~~

.. code:: py

   # Change slicing geometry to a sphere
   slice.SliceType = 'Sphere'
   slice.SliceType.Center = [1.0, 0.0, 0.0]
   slice.SliceType.Radius = 5.0

.. |image0| image:: ../../img/procedures/03SliceFilterSelection.png
.. |image1| image:: ../../img/procedures/03SliceFilterAppearance.png
.. |image2| image:: ../../img/procedures/03SliceFilterHidePlane.png
.. |image3| image:: ../../img/procedures/03SliceFilterMultiple.png
.. |image4| image:: ../../img/procedures/03SliceFilterOptions.png
.. |image5| image:: ../../img/procedures/03SliceFilterValueGenerator.png
.. |image6| image:: ../../img/procedures/03SliceBox.png
.. |image7| image:: ../../img/procedures/03SliceCylinder.png
.. |image8| image:: ../../img/procedures/03SliceSphere.png
