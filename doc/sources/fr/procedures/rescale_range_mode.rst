Documentation: `doc en <../../en/procedures/rescale_range_mode.html>`__



Remise à l'échelle du rang
==========================

Description
-----------

Au niveau de la vue d'édition de la palette de couleur (**Color Map Editor**),
il est possible de choisir entre différents modes de remise à l'échelle du rang
pour champ de valeurs en cours d'affichage.

|image0|

Ces modes sont regroupés en deux catégories :

# à droite dans la partie **Mapping Data**, quatre boutons proposent différents modes de remise à l'échelle manuel ; et

# en haut de cette vue, un menu déroulant propose différents modes de remise à l'échelle automatique lorsque l'utilisateur passe d'un temps de simulation à l'autre (**Automatic Rescale Range Mode**).

Remise à l'échelle manuel du rang
---------------------------------

|image1|

Dans la partie **Mapping Data**, à droite, quatre boutons proposent
différents modes de remise à l'échelle manuel qui sont, en les parcourant de haut en bas :

# en fonction des valeurs du champ prises pour ce temps courant (**Rescale to data range**) :

  Le choix retenu pour les valeurs minimum et maximum sont les extremum des valeurs de la donnée atteints pour le champ de valeur en cours d'affichage et en ayant parcouru les valeurs pour le pas de temps de simulation courant ;

# personnalisé (**Rescale to custom range**) :

  L'utilisateur fixe lui-même les valeurs du minimum and maximum ;

# en fonction des valeurs du champ prises pour tous les temps de simulation (**Rescale to data range over all timesteps**) :

  Le choix retenu pour les valeurs minimum et maximum
  sont les extremum des valeurs de la donnée atteints pour le champ de valeur en cours d'affichage et en ayant
  parcouru les valeurs de tous les temps de simulation.
  Une fenêtre de dialogue (titrée **Rescale range over time**) informe que la détermination de ce rang peut
  durer un certain temps. En effet, tous les temps de simulation doivent être parcourus afin de déterminer ces
  extremum. La durée sera directement liée au nombre de temps de simulation présent dans cette base comme
  des choix de chargement fait au niveau du lecteur ;

# en fonction de ce qui est visible (**Rescale to visible range**) :

  Le choix retenu pour les valeurs
  minimum and maximum sont les extremum des valeurs de la donnée atteints pour le champ de valeur en cours
  d'affichage, en ayant parcouru les valeurs de tous les temps de simulation et pour la partie visible de l'objet.

.. warning::
   Actuellement au moins en 0.0.12, le mode **Rescale to visible range** semble poser quelques problèmes.

L'application d'un de ces quatre modes désactive les mises à jour automatique lorsque l'utilisateur change de
temps de simulation. ***Never*** (jamais) est alors positionné comme choix de remise à l'échelle automatique
**Automatic Rescale Range Mode**.

Remise à l'échelle automatique du rang
--------------------------------------

En haut de cette vue est proposé un menu déroulant proposant cinq modes de remise à l'échelle
automatique lorsque l'utilisateur passe d'un temps de simulation à un autre (**Automatic Rescale Range Mode**).

Never
^^^^^

Le positionnement à ***Never*** permet de désactiver les mises à jour automatique du rang.

Grow and update on 'Apply'
^^^^^^^^^^^^^^^^^^^^^^^^^^

Ce mode permet de modifier en agrandissant le rang courant de façon automatique afin de prendre
en compte toutes nouvelles valeurs qui auraient été initialement en dehors du rang
lorsque l'utilisateur clique sur le bouton 'Apply'.

Ainsi, le rang s'étend en diminuant la valeur attribuée au minimum au fur et à mesure que
l'on découvre des valeurs plus petites.

De même, le rang s'étend en augmentant la valeur attribuée au maximum au fur et à mesure que
l'on découvre des valeurs plus grandes.

Grow and update every timestep
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le comportement de ce mode est équivalent que l'option précédent **Grow and update on 'Apply'**,
cependant la remise à l'échelle est appliquée lors de tout changement de temps de simulation
par l'utilisateur.

Un parcours de tous les temps de simulation par l'utilisateur en ayant activé ce mode de remise
à l'échelle automatique produit le même résultat qu'un clic sur le bouton
**Rescale to data range over all timesteps**,une des options de remise à l'échelle manuelle.

Update on 'Apply'
^^^^^^^^^^^^^^^^^

Le choix retenu pour les valeurs minimum et maximum sont les extremum des valeurs de la donnée
au temps de la simulation courant à l'instant où l'on a cliqué sur le bouton 'Apply'.

Clamp and update every timestep
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le comportement est identique à l'option **Update on 'Apply'** mais appliqué lorsque l'utilisateur
change de temps de simulation.

Scripting Python
----------------

1. Activer un **Automatic Rescale Range Mode**

..code:: py
    # Get color transfer function/color map for 'EQPS'
    eQPSLUT = GetColorTransferFunction('EQPS')

    # Properties modified on eQPSLUT
    eQPSLUT.AutomaticRescaleRangeMode = 'Grow and update every timestep'

Ce script permet de modifier en agrandissant le rang courant de façon automatique afin de prendre
en compte toutes nouvelles valeurs qui aurait été initialement en dehors du rang.

2. Activer un **Set a Rescale to custom range**

..code:: py
    # Get color transfer function/color map for 'EQPS'
    eQPSLUT = GetColorTransferFunction('EQPS')

    # Rescale transfer function
    eQPSLUT.RescaleTransferFunction(1.0, 2.0)

Ce script permet de positionner le rang a [1., 2.] ce qui positionne la remise à l'échelle automatique
(**Automatic rescale range mode**) à jamais (***Never***).

.. |image0| image:: ../../img/procedures/ColorMapEditor.png
.. |image1| image:: ../../img/procedures/ColorMapEditor_ManualRescale.png
