Documentation: `doc en <../../en/procedures/axes.html>`__



Axes
====

Paraview propose différents modes de rendu des axes.

Dans les propriétés (**Properties**), il vous suffit de saisir **axe**
dans le champ de recherche en ayant bien activé le mode avancée en cliquant
sur la *roue dentée* à droite de ce champ de saisie.

Data Axes Grid
--------------

Dans la catégorie **Display** (affichage), nous trouvons la possibilité d'afficher le
**Data Axes Grid** qui va construire la représentation des axes sur la boîte
englobante de la source active dans le **Pipeline Browser**
(navigateur de pipeline). Un bouton **Edit** est associé afin de changer
différentes propriétés de cet objet.

Axes Grid
---------

Dans la catégorie **View (Render View)** (vue), nous trouvons la possibilité
d'afficher le **Axes Grid** qui va construire la représentation des axes
en tenant compte de toutes les instances d'objet visualisés dans le
**Pipeline Browser** (navigateur de pipeline). Une instance est visualisée
si l'oeil à sa gauche est coché.
Un bouton **Edit** est associé afin de changer différentes propriétés de cet objet.

Polar Axes
----------

Dans la catégorie **Annotations**, nous trouvons la possibilité d'afficher le
**Polar Axes** accompagné d'un bouton **Edit** pour accéder aux propriétés de
cet objet.

Un des paramètres proposés permet de définir un **Ratio** au niveau
de la catégorie **Aspect Control** afin d'obtenir un aspect elliptique.

De même, l'option **Screen Size** permet de gérer globalement le rendu des textes.
