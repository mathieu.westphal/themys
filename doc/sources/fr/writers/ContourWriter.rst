Documentation: `doc en <../../en/writers/ContourWriter.html>`__

Ecrivain de fichier de contours au format DAT
=============================================

Description
-----------

Le filtre **ContourWriter** permet d’écrire un fichier de type **.dat** contenant
l'ensemble des contours des différents maillages présent dans la source
sélectionnée dans le **Pipeline**.

Usage
-----

Le déclenchement de l'écriture se fait en sélectionnant **File->Save Data** dans la GUI, ou via
le raccourci clavier correspondant (**CTRL^S**) ou encore en cliquant sur [l'icône](|image0|).

Dans la fenêtre qui s'ouvre, il faut sélectionner **Contour Files(*.dat)** dans le champ **Files of type:**

|image1|

Puis, après avoir choisi l'endroit ou écrire le fichier de contour, il faut entrer le nom souhaité (avec ou sans extension)
puis cliquer sur **OK**.

Le format .dat
--------------

Le format de fichier **.dat** contient une liste de poly-lignes dans un fichier au format **ASCII**.

Chaque ligne du fichier contient les coordonnées d'un des points d'une poly-ligne.

Les poly-lignes sont séparées par le caractère **&**, seul sur une ligne.

Tous les points d'une poly-ligne sont reliés entre eux dans l'ordre dans lequel ils apparaissent dans le fichier.

Voici un exemple de fichier **.dat** décrivant deux poly-lignes.

::

   -1.0000000000 0.0000000000
   0.0000000000 1.0000000000
   1.0000000000 0.0000000000
   0.0000000000 -1.0000000000
   &
   -2.0000000000 0.0000000000
   0.0000000000 2.0000000000
   2.0000000000 0.0000000000
   &

Chaque ligne décrit :

* soit un point de l’espace par un tuple de deux ou trois valeurs flottantes ;

* soit un séparateur **&** positionné en fin de la description d'une poly-ligne.

Le filtre **CEAReaderDAT** permet de lire ce format.

Exemple
-------

L'image ci-dessous superpose un maillage non simplement connexe (gris) et le contour obtenu (ligne blanche):

|image2|

Le fichier de contour correspondant est le suivant:

::

   0.8000000119 0.5199999809 0.0000000000
   0.7799999714 0.5199999809 0.0000000000
   0.7799999714 0.5000000000 0.0000000000
   0.7799999714 0.4799999893 0.0000000000
   0.7599999905 0.4799999893 0.0000000000
   0.7599999905 0.4600000083 0.0000000000
   0.7400000095 0.4600000083 0.0000000000
   ...
   0.8199999928 0.5799999833 0.0000000000
   0.8199999928 0.5600000024 0.0000000000
   0.8199999928 0.5400000215 0.0000000000
   0.8000000119 0.5400000215 0.0000000000
   0.8000000119 0.5199999809 0.0000000000
   &
   0.5000000000 0.5400000215 0.0000000000
   0.5000000000 0.5199999809 0.0000000000
   0.4799999893 0.5199999809 0.0000000000
   0.4799999893 0.5400000215 0.0000000000
   0.5000000000 0.5400000215 0.0000000000
   &
   0.4399999976 0.5400000215 0.0000000000
   0.4399999976 0.5199999809 0.0000000000
   0.4199999869 0.5199999809 0.0000000000
   0.4199999869 0.5400000215 0.0000000000
   0.4399999976 0.5400000215 0.0000000000
   &
   0.4199999869 0.4600000083 0.0000000000
   0.4000000060 0.4600000083 0.0000000000
   0.4000000060 0.4799999893 0.0000000000
   0.4000000060 0.5000000000 0.0000000000
   0.4199999869 0.5000000000 0.0000000000
   0.4199999869 0.4799999893 0.0000000000
   0.4199999869 0.4600000083 0.0000000000
   &
   0.4399999976 0.5000000000 0.0000000000
   0.4600000083 0.5000000000 0.0000000000
   0.4600000083 0.4799999893 0.0000000000
   0.4600000083 0.4600000083 0.0000000000
   0.4600000083 0.4399999976 0.0000000000
   0.4799999893 0.4399999976 0.0000000000
   0.4799999893 0.4600000083 0.0000000000
   0.4600000083 0.4600000083 0.0000000000
   0.4399999976 0.4600000083 0.0000000000
   0.4399999976 0.4799999893 0.0000000000
   0.4399999976 0.5000000000 0.0000000000
   &
   0.6000000238 0.5000000000 0.0000000000
   0.6200000048 0.5000000000 0.0000000000
   0.6200000048 0.5199999809 0.0000000000
   0.6000000238 0.5199999809 0.0000000000
   0.6000000238 0.5000000000 0.0000000000
   &

On y retrouve 6 poly-lignes, une pour chacun des contours du maillage, illustrés sur la figure ci-dessous:

|image3|

.. |image0| image:: ../../img/writers/pqSave.svg
  :width: 16
.. |image1| image:: ../../img/writers/saveFile.png
  :width: 400
.. |image2| image:: ../../img/writers/example.png
  :width: 800
.. |image3| image:: ../../img/writers/example_contours.png
  :width: 800
