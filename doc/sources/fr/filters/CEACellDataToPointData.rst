Filtre CEA CellDataToPointData
==============================

Exemple d'utilisation
---------------------

Une utilisation de ce filtre est de construire une interface d'un matériau/maillage
à partir d'un champ **aux cellules** représentant la fraction volumique de ce matériau.

Cette interface est obtenue en appliquant le filtre ``IsoVolume`` sur un champ **aux noeuds**
représentant une fraction de présence. Ce champ **aux noeuds** est obtenu par projection
**aux noeuds** du champ ***aux cellules** de la fraction volumique, intialement fourni
par le code, grâce à l'application du filtre ``CEACellDataToPointData``.

On construit le ``Pipeline`` suivant :

* ouverture d'une bande contenant un maillage structuré régulier/eulérien
  comprenant des cellules mixtes ;

* chargement du champ de valeurs aux cellules qui correspond à
  une ``fraction volumique`` ;

* application du filtre ``CEA CellDataToPointData``,
  projection du champ de valeurs ``fraction volumique`` des
  cellules aux noeuds, avec les valeurs des options suivantes :

  * ``WeightCellOption`` = ``Quad`` ;
  * ``BoundaryConditionPoint`` = ``Y Axis`` ;
  * ``AxisAlignmentBoundaryConditionPoint`` = 0; et
  * ``AbsoluteErrorEpsilonOnAxisAlignment``, on conserve la
    valeur par défaut ;

* application du filtre ``iso-volume`` sur ce nouveau champ
  ``fraction volumique`` présent aux points ;

* pour finir, par l'application du filtre ``Symetry`` suivant l'axe Y.

Vous obtenez ce résultat :

|image8|

A gauche, on retrouve le ``Pipeline Browser`` correspondant.

En dessous, dans ``Propriétés``, nous retrouvons les paramètres pour
l'application de l'``IsoVolume``. Logiquement, on s'attendrait à positionner
comme valeur maximale du Threshold la valeur 1, sauf qu'on constate que le
résultat ne correspond pas à notre attente et qu'il est nécessaire de mettre
une valeur supérieure à 1 comme 1.1. Il semblerait que le problème vienne
d'une erreur d'arrondi lors du calcul de la projection de la valeur des cellules
aux noeuds.

Ici, au lieu d'utiliser le filtre de ``Symetry`` nous avons opté pour utiliser
le filtre ``Transform`` (avec une rotation à 180° en Y et Z).

A gauche, nous avons deux représentations :

* le rendu de gauche correspond au résultat complet que l'on obtient ; la coloration
  se fait suivant les valeurs du champ ``Milieu:Density`` ; à remarquer deux choses :

  * les contours extérieurs ne sont pas "droits" ; c'est parce qu'ils n'ont pas
    été pris en compte par la condition ``BoundaryConditionPoint`` que l'on a
    appliqué que sur ``Y axis`` ; en effet, ce filtre ne permet, en l'état, de ne
    prendre en compte qu'une condition ;
  * des trous apparaissent au centre de ce maillage, ils sont du au traitement
    inadapté dans le cas de cellules mixtes comportant plus de deux matériaux ;

* le rendu de droite correspond à ce même résultat dont on a retiré l'affichage
  via la vue ``MultiBlock Inspector`` de deux des 4 matériaux. On remarque ainsi
  mieux l'aspect lissé de l'interface.

Description
-----------

Le filtre VTK générique ``CellDataToPointData``
permet de réaliser la projection d'un champ de valeurs G décrit
initialement aux centres des cellules vers les points.
A l'issu de l'application de ce filtre, un nouveau champ de
valeurs G apparait au niveau des points.
Sans plus de considération, cette façon de faire
est exacte et conforme à ce qu'on en attend.
Ce filtre peut
s'appliquer sur plusieurs champs de valeurs aux cellules.
Le calcul d'un nouveau champ en un point n'est autre qu'une
moyenne appliquée sur les valeurs des cellules qui comprennent
ce point.

Cette façon de faire devient moins évidente si le maillage
concerné est une partie d'un maillage de simulation plus
grand ; c'est ce que l'on fait généralement en présence
de cellules mixtes dans le maillage de simulation.
Une cellule mixte comporte plusieurs matériaux faisant
ainsi intervenir la notion de fraction de présence
(surfacique en 2D ou volumique en 3D) d'un matériau ainsi
que la notion de valeur partielle associée à un matériau
(comme la pression partielle pour chacun des
matériaux) ; bien souvent en plus d'une valeur globale
associée à la cellule mixte toute entière.
Il est naturel alors de produire autant de
sous-maillages qu'il y a de matériau.

Pour les maillages de simulation de type structuré, la version
de ce filtre CEA nommé ``CEACellDataToPointData`` propose
un calcul alternatif nécessitant de préciser le type des
cellules présents dans le maillage structuré sous-jacent.
Le nouveau calcul exploite alors une pondération fixée lors
de cette projection.

Option de modification du poids de chaque cellule
-------------------------------------------------

Le nom de cette option est ``WeightCellOption`` qui propose
trois valeurs : ``Standard``, ``Quad`` et ``Hexahedron``.

Raison
~~~~~~

Cette option est particulièrement utilisée sur la projection aux
points d'un champ représentant la fraction de présence d'un matériau
(valeur surfacique ou volumique).

Si le maillage sous-jacent est un maillage structuré régulier
ne comportant que des ``Quad`` ou des ``Hexahedron`` alors la
valeur de cette fraction projetée aux noeuds aura une valeur cohérente
au niveau d'une cellule présente sur deux maillages décrivant des
matériaux différents. La valeur ainsi obtenue, comme celles utilisées,
est comprise entre [0. ; 1.]. Il semblerait néanmoins que cette valeur
numérique informatique soit trés légérement supérieure à 1.

L'avantage de cela, c'est que ce nouveau champ pourra être alors utilisé
lors de l'application d'un filtre d'iso-Contour 2D ou d'iso-Volume 3D
afin de construire une unique interface entre deux matériaux.

Attention néanmoins à l'exploitation de ce nouveau champ, en effet,
l'intervalle à retenir ne sera pas [0.5 ; 1.0] mais
plutôt [0.5 ; 1.1] afin de prendre en compte cette toute petite erreur
d'arrondis remarqués précédemment.

Attention néanmoins, cette façon de produire une interface ne
fonctionne pas pour les cellules mixtes à plus de deux matériaux
(vous verrez plus loin que cela fait apparaître des "trous").

On comprend bien que cette option ne peut s'appliquer que
sur des maillages qui sont une partie d'un maillage structuré plus
grand, dont on suppose, complétement  ou sur des maillages de simulation
non conforme à la condition décrite plus haut.

Mécanisme
~~~~~~~~~

Le filtre ``CEACellDataToPointData`` permet de modifier le poids
que l'on attribue à chaque cellule en considérant arbitrairement
que le maillage actuel est un maillage construit comme une sélection
d'un maillage structuré régulier basé sur des ``Quad`` en 2D ou
des ``Hexahedron`` en 3D.

L'option ``WeightCellOption`` propose les valeurs suivantes :

* ``Standard`` afin de retrouver le résultat proposé par le filtre
  original ``CellDataToPointData`` ;

* ``Quad`` lorsqu'on considère le maillage sous-jacent comme étant
  régulier 2D constitué de cellules ``Quad`` ;

* ``Hexahedron`` lorsqu'on considère le maillage sous-jacent comme étant
  régulier 3D constitué de cellules ``Hexahedron``.

En faisant le choix ``Quad``, resp. ``Hexahedron``, à chaque point, la
somme obtenue des contributions des cellules sera divisée par 4,
resp. 8, au lieu du nombre de contributions.
En agissant de la sorte, les cellules voisines non décritent dans une
partie du maillage de simulation seront néanmoins considérées
comme ayant la valeur nulle.

Exemple
~~~~~~~

Un cas d'exemple de projection avec le mode standard,
la valeur de l'option ``WeightCellOption`` = ``Standard`` ce qui
correspond au défaut de ``CellDataToPointData``, suivi par
l'application du filtre d'iso-contour sur ce nouveau champ de valeurs
aux noeuds [0.5 ; 1.1].
Nous avons affiché le matériau du milieu en filaire afin de voir ce qu'il
se passe aux frontières avec le matériau du haut puis du bas.

|image0|

On constate clairement que la frontière déborde sur les matériaux voisins.

Voici la même chose mais en optant pour une projection considérant
le maillage de simulation comme structuré régulier et composé de quad,
la valeur de l'option ``WeightCellOption`` = ``Qaud``.

|image1|

Cette option permet de supprimer les défauts relevés dans la mode standard
au niveau des frontières.
De plus, on observe que l'interface est naturellement plus lissée.

Par contre, et on ne pourra rien y faire, la limitation signalée plus
haut est évidente lorsqu'on regarde au centre de ce maillage
de simulation qui comporte des cellules mixtes à plus de deux matériaux :
des trous apparaissent. La raison est assez simple, il n'est pas possible de
produire trois parties de la cellule avec un unique plan d'interface (c'est
ce que fait le filtre d'iso-contour ou d'iso-volume).

|image2|

Option de condition limite aux points
-------------------------------------

Le nom des options concernées sont ``BoundaryConditionPoint``,
``AxisAlignmentBoundaryConditionPoint`` et
``AbsoluteErrorEpsilonOnAxisAlignment``.

Raison
~~~~~~

L'application de l'option précédente ``WeightCellOption`` avec une valeur
autre que ``Standard`` a pour effet de considérer le maillage sous-jacent
comme infini. Ce qui se traduit par considérer, au-delà des limites
du maillage sous-jacent des cellules ayant la valeur nulle.

Cela pose un problème de rendu surtout lorsqu'on veut appliquer par la suite
un filtre de symétrie à des fins de comparaison ou simplement pour
reconstitué artificiellement un objet complet. En effet, une non-continuité
sur la valeur apparaît alors qu'avec le mode de projection ``Standard``
elle n'y était pas.

Mécanisme
~~~~~~~~~

L'idée est alors de désactiver le mode de projection pour revenir au
mode ``Standard`` uniquement pour les noeuds que l'on déclare être sur une
condition aux limites.

A travers l'option ``BoundaryConditionPoint``, on propose de définir
une axe particulier :

* ``None``;

* ``X Axis``;

* ``Y Axis``;

* ``Z Axis``.

Puis on peut fixer l'offset suivant cet axe à travers l'Option
``AxisAlignmentBoundaryConditionPoint`` qui prend, par défaut, la valeur 0.

Pour finir, afin de s'astreindre des contraintes numériques, l'option
``AbsoluteErrorEpsilonOnAxisAlignment`` permet de définir une erreur
absolue lors de la recherche de ces points.

Exemple
~~~~~~~

Ce cas d'exemple comporte une projection aux points suivi de l'application
du filtre d'iso-volume sur ce nouveau champ de valeurs aux noeuds [0.5 ; 1.1],
suivi de l'application d'un filtre de symétrie tout en conservant l'affichage
du maillage avant symétrie.

Dans un premier temps, regardons de plus près l'effet du mode de projection
au bord suivi d'un filtre d'iso-volume.

Voici le résultat en mode ``Standard`` :

|image3|

puis en mode ``Quad`` :

|image4|

On remarque alors l'intérêt de ce mode qui permet de mieux lisser et faire
correspondre les interfaces entre deux maillages représentant de matériaux
de la simulation.

Par contre, on constate :

* un morceau disparaît lors de l'application du mode ``Quad`` ; cela
  s'explique parce que la fraction de présence est inférieue à 0.5 ;

* une décoloration du maillage au bord toujours en mode ``Quad`` ce
  qui est normal en considérant qu'il y a des cellules voisines à
  valeurs nulles.

Si l'on applique alors un filtre de symétrie toujours en mode ``Quad`` nous
obtenons ceci :

|image5|

ce qui met en avant ces deux défauts.

L'application d'une condition limite avec les options ``BoundaryConditionPoint``
à ``Y Axis`` et ``AxisAlignmentBoundaryConditionPoint`` à 0. permet
de désactiver le mode ``Quad`` (pour revenir au mode ``Standard``) que pour
le calcul des points ayant, ici, une ordonnée nulle :

|image6|

ce qui donne avec l'application du filtre de symétrie un résultat beaucoup
plus attendu, le meilleur des deux :

|image7|

On obtient alors le bénéfice du mode de projection ``Quad`` tout en
gardant, au moins suivant la condition limite exprimée ici en Y=0, un rendu
visuel correct sans non-continuité de la valeur.

.. |image0| image:: ../../img/filters/celldatatopointdata_standard_projection.png
.. |image1| image:: ../../img/filters/celldatatopointdata_quad_projection.png
.. |image2| image:: ../../img/filters/celldatatopointdata_quad_projection_abovetwo.png
.. |image3| image:: ../../img/filters/celldatatopointdata_quad_projection_without_symetry.png
.. |image4| image:: ../../img/filters/celldatatopointdata_standard_projection_without_symetry.png
.. |image5| image:: ../../img/filters/celldatatopointdata_quad_projection_with_symetry.png
.. |image6| image:: ../../img/filters/celldatatopointdata_quad_projection_and_boundary_without_symetry.png
.. |image7| image:: ../../img/filters/celldatatopointdata_quad_projection_and_boundary_with_symetry.png
.. |image8| image:: ../../img/filters/celldatatopointdata_quad_projection_and_boundary_with_symetry_intro.png
