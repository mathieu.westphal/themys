Filtre Custom Line Intergator
=============================

Description
-----------

Le filtre ``CustomLineIntergator`` permet de calculer l’intégration
d’une valeur sur une polyligne de plusieurs façons.

Option
------

Méthodes de calcul
~~~~~~~~~~~~~~~~~~

La méthode de calcul varie suivant l’opérateur retenu :

-  ``cumsum(Fi)`` retourne la somme cumulative des valeurs du champ de
   valeurs des cellules au long de la polyline dans deux nouveaux champs
   de valeurs ``Integral`` et ``GlobalIntegral`` ;

-  ``cumsum(Fi*dLi)`` retourne la somme cumulative des valeurs du
   produit du champ de valeurs par la longueur de traversée dans la
   cellule des cellules au long de la polyline dans deux nouveaux champs
   de valeurs ``LinearIntegral`` et ``GlobalLinearIntegral`` ; le champ
   de valeurs qui décrit ``dLi`` est un champ nommé ``arc_length`` ;

-  ``custom`` retourne le calcul décrit par l’utilisateur dans deux
   nouveaux champs de valeurs ``Custom`` et ``GlobalCustom``.
