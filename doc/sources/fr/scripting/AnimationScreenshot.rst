Create and save screenshots for temporal data sets
==================================================

Description
-----------

Temporal data sets may move or change shape between different timesteps.
As a result, when producing a screenshot at an arbitrary timestep, the
camera may not always properly capture the entire data set.

For this reason, it is preferable to place the camera at an adequate
position before saving an image of the view. This can be done manually
by specifying a position or automatically by centering the camera to fit
the data.

This process will be described using Python scripting as it is
especially convenient to iterate through each timestep. For more details
on how to execute a Python script, please refer to `this
page <./ScriptExecution.md>`__.

Procedure
---------

1. Creating a single screenshot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This subsection describes the main steps to produce one screenshot by:

-  selecting the view and source data to produce a screenshot of

-  moving to the desired timestep

-  placing the camera at an adequate position (doing so manually
   requires specific coordinates and is not very convenient)

-  saving a screenshot at a specified path location and image resolution

The script to perform these steps is the following:

.. code:: py

   # Get active view
   view = GetActiveViewOrCreate('RenderView')

   # Retrieve source data
   source = GetActiveSource()

   # Change time to the desired timestep (e.g. timestep 10)
   view.ViewTime = source.TimestepValues[10]

   # Reset view to fit and center data
   view.ResetCamera()
   # OR
   # Place the camera manually
   view.CameraPosition = [30.0, 30.0, 30.0]
   view.CameraFocalPoint = [0.0, 0.0, 0.0]
   view.CameraViewUp = [0.0, 0.0, 1.0]

   # Save a screenshot of the view with specified resolution
   SaveScreenshot('/path/to/screenshot/MyScreenshot.png', view, ImageResolution=[1920, 1080])

2. Creating one screenshot per timestep with a fixed camera position
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Obtaining one screenshot for each timestep can be done by iterating
through all timesteps with the use of a ``for`` loop described below.

.. code:: py

   # Get active view
   view = GetActiveViewOrCreate('RenderView')

   # Retrieve source data
   source = GetActiveSource()

   # Reset view to fit and center data on the first timestep
   view.ResetCamera()
   # OR
   # Place the camera manually
   view.CameraPosition = [30.0, 30.0, 30.0]
   view.CameraFocalPoint = [0.0, 0.0, 0.0]
   view.CameraViewUp = [0.0, 0.0, 1.0]

   # Loop over all timesteps
   for step in range(len(source.TimestepValues)):

     # Update timestep
     view.ViewTime = source.TimestepValues[step]

     # Save a screenshot of the view with specified resolution
     SaveScreenshot('/path/to/screenshot/MyScreenshot' + str(step) + '.png', view, ImageResolution=[1920, 1080])

Alternatively, image extractors can also be used to produce images at
each timestep. Extractors automatically iterate through the timesteps
while creating images.

Simply replace the ``for`` loop above with the following:

.. code:: py

   #  Create a PNG extractor
   extractor = CreateExtractor('PNG', view)

   # Optionally set a name pattern for the images
   # extractor.Writer.FileName = 'RenderView1_{timestep:06d}{camera}.png'

   # Optionally set image resolution
   # extractor.Writer.ImageResolution = [1920, 1080]

   # Execute the extraction
   SaveExtracts(ExtractsOutputDirectory='/path/to/screenshot/folder')

3. Creating one screenshot per timestep with a centered camera
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Given that the data object can move between timesteps, you may wish to
recenter the camera after moving to the next timestep. This can be
achieved with the following script.

.. code:: py

   # Get active view
   view = GetActiveViewOrCreate('RenderView')

   # Retrieve source data
   source = GetActiveSource()

   # Loop over all timesteps
   for step in range(len(source.TimestepValues)):

     # Update timestep
     view.ViewTime = source.TimestepValues[step]

     # Reset view to fit and center data
     view.ResetCamera()

     # Save a screenshot of the view with specified resolution
     SaveScreenshot('/path/to/screenshot/MyScreenshot' + str(step) + '.png', view, ImageResolution=[1920, 1080])
