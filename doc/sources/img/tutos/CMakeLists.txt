include(mirror_files)

set(RAW_IMG_FILES "")
# RAW_IMG_FILES will be filled by each CMakeLists.txt found under this directory
# if the subdirectory holds images (not managed by sphinx, for example included
# in raw html files)

add_subdirectory(animation)
add_subdirectory(scripting)
add_subdirectory(themys)

mirror_files("${RAW_IMG_FILES}" ${CMAKE_SOURCE_DIR}/doc/sources/img
             ${DOCUMENTATION_BUILD_DIR_FR} "images_fr")
mirror_files("${RAW_IMG_FILES}" ${CMAKE_SOURCE_DIR}/doc/sources/img
             ${DOCUMENTATION_BUILD_DIR_EN} "images_en")
