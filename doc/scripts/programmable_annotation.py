from datetime import date

date = "Date %s  %f\n" % (date.today(), numpy.cumsum(inputs[0].PointData["RTData"])[-1])

to = self.GetTableOutput()
arr = vtk.vtkStringArray()
arr.SetName("Text")
arr.SetNumberOfComponents(1)
arr.InsertNextValue(date)
to.AddColumn(arr)
