option(BUILD_SHARED_LIBS "Build shared libraries" ON)

set(plugin_files # ClientOnlyPlugins/ThemysDocumentation/paraview.plugin
    ClientOnlyPlugins/ThemysMinMaxSelection/paraview.plugin)

file(GLOB serverplugins_content "${CMAKE_CURRENT_LIST_DIR}/Plugins/*")
if(${USE_SERVERPLUGINS})
  if(NOT serverplugins_content)
    message(
      FATAL_ERROR
        "Using server plugins requires the submodule has been checked out. It doesn't seem to be the case!\n"
        "Please run 'git submodule update --init ---recursive' in the project directory to check out the submodule\n"
        "Otherwise use the -DUSE_SERVERPLUGINS=OFF option to confirm you want to build themys without server plugins."
    )
  endif()
  list(APPEND plugin_files Plugins/ThemysSettings/paraview.plugin
       Plugins/ThemysFilters/paraview.plugin
       Plugins/ThemysRepresentations/paraview.plugin)
else()
  if(serverplugins_content)
    message(
      FATAL_ERROR
        "Using server plugins is deactivated (USE_SERVERPLUGINS = OFF) but the submodule seems to have been checked out.\n"
        "If you really want to build Themys without serverplugins please remove files in Plugins directory.\n"
        "Aborting for security reason.")
  endif()
endif()

paraview_plugin_scan(
  PLUGIN_FILES
  ${plugin_files}
  PROVIDES_PLUGINS
  plugins
  ENABLE_BY_DEFAULT
  ON
  HIDE_PLUGINS_FROM_CACHE
  ON)

paraview_plugin_build(
  PLUGINS
  ${plugins}
  AUTOLOAD
  ${plugins}
  PLUGINS_FILE_NAME
  "themys.xml"
  INSTALL_EXPORT
  ThemysParaViewPlugins
  RUNTIME_DESTINATION
  ${CMAKE_INSTALL_BINDIR}
  CMAKE_DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/Themys
  LIBRARY_SUBDIRECTORY
  "themys"
  TARGET
  themys_paraview_plugins)
