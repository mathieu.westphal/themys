# Documentation plugin example

## Before building:

 - Install Sphinx
 - Write the plugin documentation in a markdown file inside one of the subdirectory
   of the `themys/doc/source` directory.
 - Add the markdown file and all necessary ressources to the `DOC_FILES` list.
 - Build `Themys` through CMake or just run: `cmake --build . --target RunSphinx`

## PS

 - The `Dummy.xml` file is necessary in order to actually generate the documentation
 - One could improve this workflow using a CMake custom_command while creating some CMake variables so the user can set wich program will translate the MD files with which arguments. This workflow is not ideal either.
