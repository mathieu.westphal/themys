#include "pqMinMaxSelection.h"

// PQ headers
#include <pqActiveObjects.h>
#include <pqApplicationCore.h>
#include <pqDataRepresentation.h>
#include <pqObjectBuilder.h>
#include <pqSelectionManager.h>
#include <pqServer.h>

// VTK SM headers
#include <vtkSMCoreUtilities.h>
#include <vtkSMPVRepresentationProxy.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMSelectionHelper.h>
#include <vtkSMSessionProxyManager.h>
#include <vtkSMSourceProxy.h>

// VTK headers
#include <vtkPVArrayInformation.h>
#include <vtkSmartPointer.h>

// Qt headers
#include <QApplication>
#include <QStyle>

//-----------------------------------------------------------------------------
void pqMinMaxSelection::findData() const
{
  // Retrieve the active server and associated proxy manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  vtkSMSessionProxyManager* pxm = server->proxyManager();

  // Retrieve the active source proxy
  pqPipelineSource* activeSource = pqActiveObjects::instance().activeSource();
  if (!activeSource)
  {
    qCritical("Failed to retrieve the active source.");
    return;
  }
  vtkSMSourceProxy* activeSourceProxy =
      vtkSMSourceProxy::SafeDownCast(activeSource->getProxy());
  if (!activeSourceProxy)
  {
    qCritical("Failed to retrieve the active source proxy.");
    return;
  }

  // Retrieve the active representation proxy
  pqDataRepresentation* activeRepr =
      pqActiveObjects::instance().activeRepresentation();
  if (!activeRepr)
  {
    qCritical("Failed to retrieve the active representation.");
    return;
  }
  vtkSMRepresentationProxy* activeReprProxy =
      vtkSMRepresentationProxy::SafeDownCast(activeRepr->getProxy());
  if (!activeReprProxy)
  {
    qCritical("Failed to retrieve the active representation proxy.");
    return;
  }

  // Retrieve the selection manager
  pqSelectionManager* selectionManager = qobject_cast<pqSelectionManager*>(
      pqApplicationCore::instance()->manager("SELECTION_MANAGER"));
  if (!selectionManager)
  {
    qCritical("Failed to retrieve the selection manager.");
    return;
  }

  // Retrieve the active array info and the LUT
  vtkPVArrayInformation* activeArrayInfo =
      vtkSMPVRepresentationProxy::GetArrayInformationForColorArray(
          activeReprProxy);
  if (!activeArrayInfo)
  {
    selectionManager->select(nullptr);
    return;
  }
  vtkSMProxy* lutProxy = activeRepr->getLookupTableProxy();

  // Retrieve the mode - "Magnitude" or "Component" - and the associated
  // component in the second case.
  std::string mode = vtkSMPropertyHelper(lutProxy, "VectorMode").GetAsString();
  std::string component = std::to_string(
      vtkSMPropertyHelper(lutProxy, "VectorComponent").GetAsInt());

  // Create a new selection source proxy
  vtkSmartPointer<vtkSMSourceProxy> selectionSource;
  selectionSource.TakeReference(vtkSMSourceProxy::SafeDownCast(
      pxm->NewProxy("sources", "SelectionQuerySource")));
  if (!selectionSource)
  {
    qCritical("Failed to create the selection source proxy.");
    return;
  }

  // Set the element type associated to the color array
  int elementType = vtkSMPropertyHelper(activeReprProxy, "ColorArrayName")
                        .GetInputArrayAssociation();
  if (elementType > 1) // Support only POINT and CELL
  {
    qWarning("Only point and cell arrays are supported - skipping selection.");
    selectionManager->select(nullptr);
    return;
  }
  vtkSMPropertyHelper(selectionSource, "ElementType").Set(elementType);

  // Set the query
  const std::string arrayName =
      vtkSMCoreUtilities::SanitizeName(activeArrayInfo->GetName());
  const int nbOfComponents = activeArrayInfo->GetNumberOfComponents();
  std::string query;

  if (nbOfComponents == 1)
  {
    // (array == max(array)) | (array == min(array))
    query = "(" + arrayName + " == max(" + arrayName +
            "))"
            "|(" +
            arrayName + " == min(" + arrayName + "))";
  } else
  {
    if (mode == "Magnitude")
    {
      // (mag(array) == max(mag(array))) | (mag(array) == min(mag(array)))
      query = "(mag(" + arrayName + ") == max(mag(" + arrayName +
              ")))"
              "|(mag(" +
              arrayName + ") == min(mag(" + arrayName + ")))";
    } else
    {
      // (array[:,component] == max(array[:,component])) | (array[:,component]
      // == min(array[:,component]))
      query = "(" + arrayName + "[:," + component + "] == max(" + arrayName +
              "[:," + component +
              "]))"
              "|(" +
              arrayName + "[:," + component + "] == min(" + arrayName + "[:," +
              component + "]))";
    }
  }

  vtkSMPropertyHelper(selectionSource, "QueryString").Set(query.c_str());
  selectionSource->UpdateVTKObjects();

  // Set the selection to the active source
  vtkSmartPointer<vtkSMSourceProxy> appendSelections;
  appendSelections.TakeReference(vtkSMSourceProxy::SafeDownCast(
      vtkSMSelectionHelper::NewAppendSelectionsFromSelectionSource(
          selectionSource)));

  activeSourceProxy->SetSelectionInput(0, appendSelections,
                                       0); // Assume portIndex = 0

  pqOutputPort* outputPort =
      activeSource->getOutputPort(0); // Assume outputport = 0
  selectionManager->select(outputPort);
}

//-----------------------------------------------------------------------------
pqMinMaxSelection::pqMinMaxSelection(QObject* p) : QActionGroup(p)
{
  QAction* action = new QAction(QIcon(":/Themys/pqMinMax.svg"),
                                tr("Select Min and Max values"), this);
  action->setEnabled(false);
  this->addAction(action);

  QObject::connect(action, &QAction::triggered, this,
                   &pqMinMaxSelection::findData);

  QObject::connect(
      &pqActiveObjects::instance(),
      qOverload<pqDataRepresentation*>(&pqActiveObjects::representationChanged),
      [=](pqDataRepresentation* repr) { action->setEnabled(repr); });
}
