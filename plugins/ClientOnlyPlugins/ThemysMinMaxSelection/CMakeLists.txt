set(interfaces)
set(sources pqMinMaxSelection.cxx pqMinMaxSelection.h)

paraview_plugin_add_action_group(
  CLASS_NAME
  pqMinMaxSelection
  GROUP_NAME
  "ToolBar/MinMaxSelection"
  INTERFACES
  action_interfaces
  SOURCES
  action_sources)

list(APPEND interfaces ${action_interfaces})
list(APPEND sources ${action_sources})

paraview_add_plugin(
  MinMaxSelection
  VERSION
  ${CMAKE_PROJECT_VERSION}
  UI_INTERFACES
  ${interfaces}
  SOURCES
  ${sources})

target_link_libraries(MinMaxSelection PRIVATE ParaView::RemotingViews)
