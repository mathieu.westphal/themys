# Themys

[Themys](index.md) est une application customisée basée sur l'application
open-source d'analyse et de visualisation de données multiplateforme de [ParaView](https://www.paraview.org/)
de [KitWare](www.kitware.com). C'est une **ParaView's branded application**.

Les utilisateurs de [Themys](index.md)/[ParaView](https://www.paraview.org/)
peuvent rapidement créer des visualisations pour analyser leurs données à l'aide de techniques
qualitatives et quantitatives, cela en définissant un enchainement de filtres.
L'exploration des données peut être effectuée de manière interactive en 1D/2D/3D ou à l'aide de
scripts d'exécution.

L'apport de [Themys](index.md) est tout particulièrement de proposer une Interface Utilisateur Graphique (GUI)
plus adaptée à l'usage métier au [CEA DAM](http://www-dam.cea.fr/).

[Themys](index.md) se veut d'être une solution alternative à l'application d'analyse et de visualisation
historique et propriétaire [LOVE](www-hpc.cea.fr/fr/red/opensource.htm) créée en 2000 (comme pour
[ParaView](https://www.paraview.org/)) afin de répondre à un contexte grandissant d’exploitation massives
de grands volumes de données issues de codes de simulation numérique.
[ParaView](https://www.paraview.org/), tout comme [LOVE](www-hpc.cea.fr/fr/red/opensource.htm), est basé
sur la boîte à outils [VTK](vtk.org).

[Themys](index.md) a été développé par KitWare dans le cadre d'un contrat défini avec le [CEA DAM](http://www-dam.cea.fr/).

CEA, DAM, DIF, F-91297 Arpajon, France

## Dependances

 - ParaView au commit `b0483af430c4d2eaeff880a5ddead21796844b78`, disponible sur https://gitlab.kitware.com/paraview/paraview/

## Documentation

La documentation de **Themys** est accessible [ici](https://themys.gitlab.io/themys).
